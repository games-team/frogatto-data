Þ    A     $"  ­  ,D      àZ  9   áZ  &   [  /   B[     r[  #   {[  @   [  ?   à[  5    \  Q   V\  2   ¨\  U   Û\  <   1]  8   n]  E   §]     í]  	   ñ]  $   û]  K    ^  +   l^  6   ^  6   Ï^     _  	   _  :   _  6   S_  >   _  @   É_  C   
`  K   N`     `     ±`  &   Ì`  8   ó`  S   ,a     a     a     a     ·a  N   Ía     b  X   <b     b  .   ¨b     ×b     ôb  6   c  /   >c  >   nc  !   ­c  (   Ïc  '   øc      d  6   8d  z   od     êd     e  [   e     ze  ¯   e  2   >f  *   qf     f     °f  "   ¾f  d   áf  ]   Fg  >   ¤g     ãg  N   ëg     :h  Y   Uh  8   ¯h  /   èh  T   i  #   mi  l   i  $   þi  	   #j  ?   -j     mj  (   zj  >   £j  /   âj  @   k  =   Sk  %   k  :   ·k  B   òk  N   5l     l     ¤l     »l  )   @m  6   jm  /   ¡m  *   Ñm  (   üm     %n  3   6n  G   jn  @   ²n     ón     o     o     o     o  :   (o  +   co  *   o  #   ºo  D   Þo  L   #p  L   pp     ½p  
   Òp     Ýp  
   ëp      öp  !   q  +   9q  	   eq  
   oq     zq  )   q     ¶q     Æq  m   Ôq  3   Br  t   vr  >   ër  Y   *s  *   s  8   ¯s     ès     t  :    t  2   [t  #   t  6   ²t     ét  	   ït     ùt  D   u  O   Zu     ªu     ±u     Âu     Ðu  Y   áu     ;v  +   Gv     sv     v  	   ¤v  !   ®v     Ðv     êv  k   ýv  }   iw  ¡   çw     x      y  '   1y     Yy     ey     y     y     ¤y  
   ´y     ¿y     Íy     Þy     îy  b   ÿy  
   bz     mz  ~   z     	{  !   %{  $   G{  &   l{     {  -   ¦{  !   Ô{     ö{     |     |      |     /|     @|     P|  4   U|     |  O   |  )   æ|     }  @   $}     e}     n}  -   }  	   ®}  5   ¸}  	   î}  	   ø}  	   ~  >   ~  ,   K~     x~  #   ~  '   £~  N   Ë~  "     #   =  P   a  G   ²  C   ú  -   >  C   l  .   °  &   ß  >     -   E  $   s  !     '   º     â                *  1   <  8   n     §  h  ·           ;     L  b  \  1   ¿  &   ñ  
     <   #  O   `      °     Ñ     Þ     ü       -   .  3   \               «  6   »     ò  8   þ  
   7  
   B     M     ]     i     p  E        Ñ     Ö     õ  V     	   ^  !   h  d     K   ï  Z   ;  9     Y   Ð  8   *  <   c  I      G   ê  B   2  a   u     ×     ä       "   
     -  	   G  (   Q     z       &        ¶     Å     Þ     û  '        :     K     f            
         «     Ã     Ï  0   Ý  5        D     b  !   j  \     %   é       (      =   I  '     %   ¯  )   Õ     ÿ  "        @     F  1   d  (        ¿  M   Ó     !  ?   :  !   z  P     <   í  i   *  a     B   ö  X   9       8   ±  *   ê  ;     O   Q      ¡  '   Â  ]   ê  )   H  %   r  I     X   â  @   ;  *   |  L   §  *   ô  T     S   t  A   È     
  +     E   >  9     4   ¾  8   ó  A   ,  )   n  E        Þ     ò     ù  2     $   5  0   Z  &     =   ²  3   ð  0   $     U     c  0   q  9   ¢  W   Ü  P   4       '     /   Ä  8   ô     -  <   J          §     µ  p   Ä  !   5     W     j       /   ¥  X   Õ     .      K      h   ^         ä   Z   ø   $   S¡     x¡  B   ¡  7   Î¡      ¢  A   '¢  \   i¢  4   Æ¢  :   û¢  F   6£  @   }£  '   ¾£  ]   æ£     D¤  B   Ç¤  F   
¥  a   Q¥     ³¥  *   9¦  <   d¦  C   ¡¦  q   å¦  c   W§  X   »§     ¨  8   ¨  T   Ò¨  >   '©  F   f©  E   ­©  K   ó©     ?ª     Kª  $   \ª     ª     ª     ªª  2   ²ª     åª  8   üª  (   5«  D   ^«     £«  /   ¹«  (   é«  .   ¬  _   A¬  6   ¡¬  4   Ø¬  )   ­  *   7­  3   b­  M   ­  :   ä­  9   ®  '   Y®  G   ®  1   É®  9   û®  c   5¯     ¯  >   $°  !   c°  E   °  9   Ë°  P   ±  V   V±     ­±  d   É±     .²     F²  c   Ç²  	   +³  A   5³     w³  y   |³  '   ö³     ´     6´  +   I´     u´     ´     ¢´  '   ²´  :   Ú´  	   µ     µ  S   /µ     µ  7    µ  !   Øµ  3   úµ  ,   .¶  )   [¶     ¶      ¶     ®¶     À¶     Ó¶     ç¶     í¶      ·  5   .·  *   d·  <   ·  )   Ì·  
   ö·     ¸     ¸     /¸     <¸  ;   R¸     ¸  #   ­¸     Ñ¸  ,   Ø¸  G   ¹     M¹  G   Z¹  d   ¢¹     º     º     .º  k   Mº  [   ¹º  @   »     V»  (   k»     »     ©»     º»     È»     Ñ»     Ú»  U   ÷»  &   M¼  X   t¼  
   Í¼     Ø¼     å¼  4   ì¼  8   !½     Z½  C   í½     1¾  !   J¾  A   l¾     ®¾     º¾  B   ½¾  :    ¿     ;¿     G¿     S¿     o¿     ¿      ¿  !   ½¿  /   ß¿  D   À     TÀ  '   gÀ  %   À  )   µÀ     ßÀ     ïÀ     Á     Á     *Á     GÁ     WÁ  ²   rÁ     %Â      DÂ  Ê   eÂ  	   0Ã     :Ã  *   JÃ  =   uÃ  !   ³Ã     ÕÃ  "   ôÃ  7   Ä     OÄ  &   eÄ     Ä      ¢Ä  F   ÃÄ  Q   
Å  ?   \Å  :   Å  *   ×Å  &   Æ  %   )Æ     OÆ  (   ]Æ     Æ  '   ¤Æ     ÌÆ     ÝÆ  :   ïÆ  ;   *Ç     fÇ     xÇ  I   |Ç  '   ÆÇ  "   îÇ     È     0È  "   CÈ  (   fÈ  G   È  R   ×È  /   *É  $   ZÉ     É  	   É     É     ®É     ÅÉ     ËÉ  &   ×É     þÉ  N   
Ê     YÊ     aÊ     oÊ     |Ê     Ê     Ê  2   ¯Ê     âÊ  F   ïÊ     6Ë     SË  *   nË  1   Ë  3   ËË  W   ÿË  1   WÌ  !   Ì  :   «Ì  !   æÌ  7   Í     @Í     DÍ  %   MÍ  !   sÍ  &   Í  %   ¼Í  $   âÍ      Î     (Î  s   >Î  Q   ²Î  :   Ï  ?   ?Ï     Ï     Ï     Ï     ¬Ï  +   µÏ  2   áÏ     Ð  !   2Ð     TÐ     `Ð     lÐ     sÐ     Ð  #   Ð     ÀÐ     ÜÐ  
   éÐ  4   ôÐ  >   )Ñ  L   hÑ  9   µÑ     ïÑ     Ò     Ò     $Ò     >Ò     WÒ  L   pÒ     ½Ò  !   ÛÒ  X   ýÒ     VÓ  (   nÓ  <   Ó  $   ÔÓ     ùÓ  &   Ô  #   ?Ô  j   cÔ  '   ÎÔ  &   öÔ     Õ  4   -Õ  :   bÕ  #   Õ  "   ÁÕ     äÕ  %   ÷Õ  2   Ö     PÖ     ^Ö     zÖ  H   þÖ  @   G×  8   ×  a   Á×  %   #Ø  U   IØ  '   Ø     ÇØ     ÝØ  $   ëØ  H   Ù  K   YÙ  M   ¥Ù     óÙ      Ú     	Ú     Ú  "   "Ú  H   EÚ     Ú     Ú  
   °Ú  	   »Ú     ÅÚ      ×Ú  1   øÚ     *Û     6Û     >Û  D   [Û      Û     ¾Û     ÏÛ  &   áÛ  5   Ü  C   >Ü  2   Ü  /   µÜ  +   åÜ     Ý  Q   1Ý     Ý  :   Ý  ?   ÐÝ  ,   Þ  &   =Þ     dÞ     Þ  9   Þ     ×Þ     ìÞ  *   	ß     4ß     Iß     Vß     tß     ß     ß  G   ­ß     õß     à     *à     Dà     _à     pà     à     à     à     ªà     ¼à     Êà  =   Úà  9   á  d   Rá  ;   ·á     óá  T   â     fâ  :   â  .   ºâ  7   éâ  L   !ã  I   nã  E   ¸ã  E   þã  B   Dä  G   ä  f   Ïä     6å  %   »å  {   áå  h   ]æ  *   Ææ  "   ñæ     ç  %   ,ç  9   Rç  :   ç  ?   Çç  U   è  :   ]è  ?   è  :   Øè  <   é     Pé  %   oé  ^   é     ôé  /   ê  H   ;ê  #   ê  t   ¨ê  O   ë  6   më  ]   ¤ë  D   ì  @   Gì  1   ì     ºì     Ôì     èì     í  w   #í     í  &   ©í  F   Ðí  1   î     Iî  6   Yî  ,   î  E   ½î     ï  I   ï  4   cï     ï     ï     °ï      Èï     éï     ð  !   ð     7ð     Tð  *   Yð     ð  4   ¢ð     ×ð  H   óð     <ñ  5   Bñ  	   xñ  0   ñ  6   ³ñ  R   êñ  W   =ò     ò     ¡ò     ²ò  +   ¹ò  F   åò     ,ó  
   ;ó     Fó     Zó     wó     ó     ó     ¤ó     ±ó  
   ¾ó     Éó     Òó  "   Øó  /   ûó     +ô  ,   Aô     nô     ô     ô     ¨ô  A   µô  o   ÷ô     gõ  M   wõ  b   Åõ     (ö  3   Gö  "   {ö  L   ö     ëö  (   
÷  r   3÷  J   ¦÷  <   ñ÷  G   .ø  L   vø     Ãø  ?   Ðø  U   ù  U   fù  '   ¼ù  	   äù  .   îù  D   ú  9   bú  %   ú  =   Âú  @    û  (   Aû  A   jû  G   ¬û  5   ôû  "   *ü      Mü  /   nü  5   ü  T   Ôü  [   )ý  h   ý  )   îý  6   þ  #   Oþ  3   sþ  7   §þ     ßþ  Z   ûþ  #   Vÿ  .   zÿ  $   ©ÿ     Îÿ  :   áÿ  <        Y  #   r  %        ¼     Î     ã     ï  G    W   O "   §    Ê (   Ù 0    V   3 !    6   ¬ 5   ã -    >   G M       Ô 0   á        ( '   4 .   \ =    "   É    ì    ü        1    6 .   G    v 	    4    #   Ë    ï ?    M   O !       ¿    Ä -   Ý     F    N   W 4   ¦    Û K   ô    @ `   Y    º 4   Î 8   	 +   <	    h	 g   	 F   ð	 /   7
 -   g
    
 4   
    Ð
 1   é
 ¨    7   Ä    ü     D    K   J L    E   ã !   ) 3   K         E    A   á    # 5   B b   x F   Û    "    ' >   8 $   w        ª $   ³ 5   Ø B    ]   Q >   ¯ X   î    G    d 8   s    ¬    Ç 7   á E    ?   _ A    ;   á C    <   a '    H   Æ Y    ;   i *   ¥ A   Ð T       g     K   
    V    u    {      ?   ' \   g    Ä f   Ô ]   ; _    ]   ù    W Y   f    À    Ó ;   Ú 8    $   O    t    z            £    ©    · 	   Í N   ×    & .   8    g Ü  w $   T    y        ´    Á @   Ü ?    5   ] 5       É    Ø )   ó ,       J    \    c %   t _    "   ú         <     \  	   i  C   s  .   ·  :   æ  2   !! Y   T! 6   ®!    å! !   û! (   " =   F" 0   " $   µ"    Ú"    ç"    ô" 6   #    ;# H   W#     # "   ¶#    Ù#    ò# 3   $ -   6$ .   d$    $ $   °$ $   Õ$    ú$ 0   % e   >%    ¤% +   Ã% 9   ï%    )&    B& .   É& !   ø&    '    0'    @' O   S' 9   £' 3   Ý' 	   ( O   ( !   k( L   ( @   Ú( 7   ) =   S)    ) @   °) !   ñ)    * '    *    H* $   W* -   |* 3   ª* $   Þ* I   +    M+ R   l+ C   ¿+ 9   ,    =,    \, P   l, $   ½, @   â, -   #- $   Q- !   v-    -    «- F   Ê- =   .    O.    _.    f.    y.    . 1   .    Â. '   á. :   	/ =   D/ 3   / L   ¶/    0 	   0    #0    00    =0 0   Y0    0     0    ±0    Á0 $   Ò0    ÷0    1 [   1 :   p1    «1 =   22 9   p2 $   ª2 7   Ï2 $   3 !   ,3 1   N3 4   3 $   µ3    Ú3    ö3    ý3    4 O   )4 !   y4    4    ¢4    ¯4    ¼4 :   É4 	   5 !   5    05    F5    b5     r5    5    ¬5    Â5 W   á5 h   96 Q   ¢6    ô6 $   7    57 !   B7    d7 	   t7 	   ~7    7    7    ¢7    ¯7    ¼7 3   Ì7     8 !   8 h   )8    8    ®8    Ê8    é8    	9 *   9    D9    Z9    m9    t9    9    9    9    ®9 B   µ9    ø9 ^   : L   d:    ±: 7   ¾:    ö:    ; $   ;    A; 1   N;    ;    ;    ; O   ; 7   ë; 	   #< %   -< *   S< W   ~< 0   Ö< *   = H   2= ?   {= B   »= *   þ= A   )> -   k> '   > ?   Á> 0   ? #   2?    V? '   u? "   ? !   À?    â?    ï? 3   ü? 0   0@    a@ >  q@    °A    ÍA    ÝA Æ   íA    ´B *   ÓB    þB 3   C *   ?C !   jC    C !   C    ¾C 	   ×C 1   áC 7   D    KD    XD    eD 4   xD    ­D /   ºD    êD 	   úD    E    E    $E $   +E -   PE    ~E (   E    ®E 7   »E    óE $   	F C   .F 9   rF O   ¬F /   üF I   ,G !   vG '   G 7   ÀG I   øG C   BH    H 	   ¥H %   ¯H 
   ÕH '   àH    I    'I    =I    VI    ]I (   pI    I    ©I !   ÈI    êI !   úI    J    2J    OJ    eJ    uJ    J    ¤J    ÀJ    ÍJ $   ÚJ =   ÿJ    =K 	   WK !   aK R   K .   ÖK    L    L *   .L 0   YL $   L .   ¯L    ÞL $   þL    #M *   0M 1   [M    M    ©M T   ¹M    N 7   .N !   fN I   N 0   ÒN    O R   O *   rO P   O    îO /   P 7   1P 1   iP -   P 1   ÉP 7   ûP I   3Q $   }Q    ¢Q U   ÁQ F   R ?   ^R $   R @   ÃR    S R    S 4   sS 6   ¨S    ßS (   ìS =   T @   ST @   T B   ÕT C   U    \U =   yU    ·U    ÍU 	   ÝU '   çU !   V 4   1V    fV $   V 7   §V >   ßV 	   W    (W !   5W 7   WW @   W :   ÐW    X *   !X )   LX '   vX !   X @   ÀX    Y    Y    *Y L   :Y !   Y    ©Y    ¼Y    ÒY %   îY O   Z    dZ    tZ    Z F   Z    ÛZ V   ëZ    B[    U[ C   k[ *   ¯[    Ú[ $   ö[ :   \ '   V\ @   ~\ @   ¿\ ?    ] -   @] F   n] C   µ] F   ù] 3   @^ $   t^ U   ^ *   ï^ C   _ .   ^_ D   _ =   Ò_ 0   ` O   A` =   ` :   Ï` 3   
a O   >a -   a F   ¼a    b    b    #b    Bb    Xb    hb %   ob    b -   ¥b     Ób I   ôb    >c :   Tc *   c ;   ºc G   öc 9   >d C   xd !   ¼d '   Þd -   e -   4e B   be 3   ¥e .   Ùe $   f 7   -f $   ef J   f ^   Õf :   4g    og =   g .   Ìg 0   ûg -   ,h    Zh :   yh    ´h X   Áh 7   i 	   Ri Q   \i    ®i [   µi    j    -j    =j )   Vj '   j    ¨j    µj 7   Âj *   új    %k    2k 3   ?k    sk -   k !   Àk 5   âk ,   l -   El    sl    l    ¢l    ¶l    Ël    ál    èl    m 0   m    Nm 9   jm '   ¤m    Ìm    Øm    åm    n    n 0   !n '   Rn    zn    n .   ¬n @   Ûn 	   o -   &o b   To    ·o    Ío    éo d   p O   mp =   ½p    ûp %   q    7q    Mq    Zq    jq 	   zq    q !   £q    Åq =   äq    "r    /r 	   ?r $   Ir *   nr    r 7   9s    qs    s F   ¢s    és    ùs @    t 7   At    yt    t    t    ¯t    Èt !   át    u F   u E   Zu     u '   ¶u (   Þu -   v 	   5v    ?v    [v !   nv !   v    ²v "   Ìv a   ïv $   Qw    vw    w    x    +x &   ;x :   bx "   x "   Àx !   ãx '   y    -y $   Ly    qy $   y =   ¯y =   íy (   +z C   Tz $   z %   ½z %   ãz    	{ *   {    D{ 3   c{    {    ­{ :   É{ F   |    K|    a| :   h| '   £| '   Ë| '   ó|    }    4} .   T} 0   } 0   ´} $   å} 0   
~    ;~ 	   H~    R~    q~    ~    ~ '   ~    Æ~ L   Ö~    #    *    7    D 	   Q    [ +   z 	   ¦ I   °    ú     /   * .   Z 0    9   º #   ô      @   9 !   z 8       Õ    Ü !   ì !       0    O    n        ¦ Z   ¼ =    (   U +   ~    ª    ·    Ê 	   ×    á !   ô        /    H    U    b    i 	   y        £    ¹    Æ !   Ü I   þ X   H @   ¡    â    ò            7    S ;   l $   ¨     Í F   î '   5 %   ] 9       ½    Ü    ø !    L   9 '    -   ®    Ü 7   ì 0   $ !   U +   w    £ '   ¼    ä          a   / !    4   ³ !   è [   
    f 6    .   ¶    å    õ !    0   ' L   X F   ¥    ì    ù            # 3   3 	   g    q 	   ~            ¢ *   Á    ì 	   ù     3       J    f    s '    7   ® <   æ -   # .   Q 1       ² [   Ë    ' 1   4 '   f *       ¹    Ø    î C       H    ^ +   t         ³    À    ß    ì 	     8   
    C    \    i    |            ¬    ¼    Ã    Ê    ×    ä +   ë :    M   R :     $   Û B        C *   V .    6   ° H   ç D   0 9   u =   ¯ =   í B   + B   n x   ± $   * M   O K       é "       +    ;    Z    s /       ¼ -   Ø @    -   G 7   u    ­    Ì R   ë    > 4   T 0    !   º b   Ü L   ? :    n   Ç 7   6 0   n $       Ä    Ý $   ð     !   4    V -   i 6    :   Î    	 @       ` ,   s      =   ¿ 3   ý    1    8    E !   U    w     "       ½    Ü    ã       7     ,   T  3        µ  (   ¼     å  1   õ  4   '¡ 4   \¡ 6   ¡    È¡    Û¡ 	   è¡ 1   ò¡ L   $¢    q¢    ¢    ¢    ¡¢    »¢    Ê¢    Ù¢ 
   ç¢    ò¢ 	   þ¢ 	   £    £    £ .   8£ $   g£ $   £    ±£    ¾£    Ñ£    ð£ :   ý£ R   8¤    ¤ 4   ¤ L   Ð¤ !   ¥ '   ?¥ (   g¥ 6   ¥ !   Ç¥ -   é¥ O   ¦ F   g¦ .   ®¦ F   Ý¦ @   $§    e§ 0   {§ L   ¬§ =   ù§ 1   7¨    i¨ "   y¨ 7   ¨ 4   Ô¨ .   	© E   8© !   ~© .    © @   Ï© =   ª .   Nª !   }ª "   ª    Âª 1   áª :   « L   N« X   « '   ô« 4   ¬    Q¬ =   j¬ 4   ¨¬    Ý¬ N   ü¬    K­ 6   d­ %   ­    Á­ $   Ú­ /   ÿ­    /®    K®    d®    }®    ®    ¦®    ³® 7   Ì® 9   ¯    >¯    Z¯ +   j¯ 1   ¯ =   È¯    ° 0   %° -   V° -   ° =   ²° =   ð°    .± %   ;±    a±    w± 7   ±    ¼± @   Û± !   ²    >²    K²    a² 	   ²    ² +   ² !   È²    ê² I   ÷² $   A³    f³ :   v³ M   ±³ -   ÿ³    -´    4´ ,   M´ 	   z´ >   ´ X   Ã´ 3   µ !   Pµ 7   rµ    ªµ F   Ãµ    
¶ .   ¶ 6   I¶ -   ¶    ®¶ q   Ê¶ 7   <·    t· +   ·    ¿· >   Æ·    ¸ '   %¸ s   M¸ 7   Á¸ 	   ù¸    ¹ /   ¹ =   7¹ @   u¹    ¶¹ *   Ò¹ =   ý¹    ;º    Kº .   [º @   º 4   Ëº .    » F   /» *   v» 	   ¡»    «» >   ¿»    þ» 	   ¼ 	   (¼ *   2¼ M   ]¼ I   «¼ R   õ¼ @   H½ M   ½    ×½    ê½    ú½ !   ¾    ;¾ 3   W¾ A   ¾ :   Í¾ ;   ¿ ;   D¿ ;   ¿ ;   ¼¿ *   ø¿ F   #À C   jÀ    ®À (   ÍÀ C   öÀ :   :Á    uÁ g   Á 0   ùÁ $   *Â 	   OÂ ^   YÂ    ¸Â C   ÑÂ R   Ã    hÃ a   uÃ k   ×Ã k   CÄ k   ¯Ä    Å @   +Å    lÅ    Å *   Å @   ¹Å !   úÅ    Æ    #Æ 	   0Æ    :Æ    JÆ    QÆ    ^Æ 	   kÆ N   uÆ    ÄÆ 6   ÑÆ    Ç    ¯  	            Z  C     0       »             ±      5  µ           ¹    µ  ö    _    ;      Ã      e    	  Ù  g      °   Ú   z           ¾      :     ?  n  Ê      S        w        9  q      §  ô  i      ñ   Ì         ¶       Æ       ^      ]   ×       {        Ê  j           j  ½        >          è            9  à  ô  M    k      }           n   Q  Ü   s  ?   æ  "  ®            ~  Þ  E  U  ô  °    i  w    è      <  e  >           Â   b          -        ï  ç  C     ,  ã  ¦  #  >  â    î   6   ß       á  Õ   Ý   c   3    Å  X        &            :          (   ù          ;  @  h  Q  @  ¨      ±   Z  U       Û   £      1  Ð    ¨  k       /        à  #               Ø   Ò       r  >  ~   )    ¤    Ø           Þ      Ñ   ©   T  %            Ù  i       g  ¥   4  °  ñ  6      0  ¸  Ö      j    Õ  ¦    ©         Ó     ã      `            x  ¼   %     ,  ä  ä         Ú          z  ì           K  o   õ            µ    À  _  ³  ?    R  º  I      f  ¡    g                u      W  Ð   ×  [  3   r           ¨   H     !  ý  õ       Û  ¤       ÷      t          û  Ý    $   ¾   ¹  ñ  T      9   	     \    õ  (    $      G   ò         Ë      (  #  ³  0  ö     @    [  Ö                D     +  ó        Y  6  ¬      â  J            ú      é  {  K  k                   ¿  Q             ¡      /  /  y         Á  Ê       -  ¶  «   Å     m   A  Î    À  ·      :    Ý  ¢  h      P         Á  ¼  ²  þ  é  Ç          {      !           j   I         N      É   3  «      ê  V      4   ñ  9    ­   d  ³   N     f   Ð  m  <  E                 O      Ë      Û      A  ³  ¥      D       !   b  8      h  H  `        `         ª          G  y     È    |        õ  v      >          9        ¼  4  Ó        #       ý       a    a  Ã  o  K        Ú  .                          c     2          ^  Z  Ì  Ü  N      ¡      A  4    )   Ç  Ä  í  h   ì  I   Â  ²          W   ÷   x   ¥  ©  1         ¸   Ë      X  °  n            Ø  §     g   6    A       þ   ò        _  J  T  ´  '   æ  Ç  ¯          Æ    º          M  Y  ©  Ù                  ×    r                    þ  -    à     Ò            X       ß      Ò  V                  ¹    ¾  ã   Ç                        á      â      *           v          Ñ  M              B       p      þ  ·      ý  å  À              V  t           .          [  å   ~                  p   0        Û      Ü          Ï      =   è  	         Þ      ~       c  ×      ¦   R   º         G                §  1  Î      P  $  ä  ­  ç  6  Ó  $  G    î      &      ë  D          8      2  ü      ?  ë  ®       ù      Ö                           ¯   ë        3      v  ç      
  (  z  £  ±                     L     ¼  )  ü   q  ®  Ï   ·   Y  <  Y          Ô  3     á           Ö  ¡     \   º  í      `      '  ò  w  o  ø          t    û         »    ê              Í      ä  ü  Ô   L            Ã   Ù   Q    ö  È      u      ö        |  W  ­  U  ¬       Ï        ª      ?  J   Ì  å       Á   á  l   É  }  t  X                B      é   »      '  ê   7  ¬               Â          v   ·  £                     O        Ë  s  ù    &            l  ï         Å            Ø      F              ó  +      ÷          ¢           ½  ô   -        .   q         ¥  V   [               ¦        É  ¤  ã  x  5      e     ¿  s     ð  Í        \          Ô    b  ´  Õ      H  ½       u  ;   a  Í   .  Ú     I      î          ª       _   í          ¤  ø          ª  Ñ    S          ]          1  %   r  b  ÿ  ¾        u   Ð  ë   5  ú          i      J    #          µ      ï      K           %  7          È   &  ¶  Ä  ;  @     Ô    %  ó  F  /       ð    Î                 (      z  å  ®    M     U  Î  ¢  +      k  P  ±  ù     Æ  ^  ü          |          4          -   }     ¬              '  +  x    ÿ           a                      
     e  B    è  F     ð   L    ´   B           H   ²  D  à    ò   í           ,    :  û      Ã  d  E    8           d   ,  ]  Ü  =  î  !        ´  æ   ¸  l  ð  m        7  ì  N  ú  7  f      ¸  ÿ      E          »        ú   È  )  É    p  O                   :        d  *      c  ,   Í        û       é  Ì   ²  2    Ä   Õ  0  Å   
  l        ó     ø  y     ê        *  À                  £       Ñ  Æ      ]  â   ß      R     ;  
               =  ø                        C      =  \  8  m      @   ¹      ¿         Ó  ç                  "       *  	   §  p    R  P  «      o              
      æ      Â  Ä  q       =  &   ¶  Ï  |  w  }  {       ß            L         T  Ý  /  7     ¯  "  f  $  F   <      1             Z       Ê  s                   <   2  *  +   S                  "            Þ   ï        ÿ        .  ­  )  O  ÷  5  2   "  '  ì  A         ½      S      W  ¨      Á  y      C        Ò  ^   8     5           ¿      «  ý  n  ¢  !   "Completely unsafe for pedestrian traffic!
Workers only!" "No Admittance without authorization." "Still under construction, wear your hard hat!" (cancel) - OpenFeint and Achievement support - [3/4 SlideToPlay.com] "a fun and surprisingly deep platformer" - [4/5 AppSpy.com] "boy-howdy is this an amazing visual feast!" - [n/a TouchArcade.com] "totally worth checking out." - arcade mode, giving instant access to action-only gameplay on randomized levels - buy powerups to enhance and unlock new abilities - forgiving savegame/respawn system so you don't have to be afraid of losing progress - grab enemies with your tongue and spit them to deal damage - massive soundtrack with over an hour of original music - playful dialogue writing that tries not to insult your intelligence ... ...
Okay. ...
That takes some getting used to. ... and then just talk, peaceably, here?
I still don't get it.  What gives? ... most of which seem inclined to kill me. ... well, at any rate,
it's good to have that settled. ... what the kitties did there
was decidedly impolite. ...And? ...Sorry. ...and I guess you have these
ants in cages to study them? ...and got my hands on a Force Talisman.
For your use. ...and mostly we just humor him,
because he's pretty harmless. ...but the subject's going to be:
"What happens to trespassers." ...he didn't have a clue what to do with it,
so he just ... hid it. ...the rest of us would still appreciate
some sort of public-works project. A Long Plastic Hallway A moment of your time sir? A pleasure to meet you.
I am Mortimer. A special button will appear,
whenever this is possible. A very useful fact is that you can control
the direction in which enemies are spat. AAAAAAH! I CAN'T FLY! ARCADE Absolutely!
Thanks again! Achievement Unlocked! Actually, nevermind - I'm just trying
to figure out what their intentions are. Added a bunch of interior props Added a few new monsters, like the red hornbugs, and completely rewrote how bats behave. Added arcade mode. Added automatic language selection for Windows Added customizable controls. Added half-hearts. Added music and sound volume controls to pause screen. Added new palettes to forest and seaside areas. Added option to reverse positions of A and B buttons (iPhone). Added scons script for compiling. Added some unique enemies to the forest. Added support for "darkness" in levels. Added support for WVGA. Added support for repeating levels (for arcade modes). Added translation support, with 4 complete translations: French, Brazilian Portuguese, Chinese (Simplified), and Japanese. Against Milgram? Really? Ah, I see..
carry on then. Ah, that.  I went and decided that
it's probably a very good idea to
do a little diplomacy. Ah, there we are... Ah, yeah, I help Chopple at taking care of the store.
He had asked me to go to the village
to know what was going on, with all that noise
that was coming from beyond the cave. Ahh, good, you made it
through there in one piece! Ahh, it's a collection of old fairy tales. Ahh, there you are! All the time. Almost all of which are his fault. Also, the tongue can grab useful items, like keys.
This is how you carry them to the lock they open. Although there's more to playing the game,
the rest, we can safely let you learn on your own. Amazing...  if you don't mind,
I have a few questions for you. Ambush! And I don't think there's a guard next
to it, whom you can steal the key from. And I get up there... how? And I'd be happy to consider some kind of project.
You yourself could be my paid liaison. And don't worry - if you die,
you'll restart right here. And don't worry, none of us here work security. And from the conversation I just overheard,
I KNOW you have something to do with it. And now he's refusing to repay you? And once they figured that out,
some of our security guys were
a little overzealous about recovering assets. And thanks for the gift,
by the way. And what? And yes, this applies to the question
marks on this level, too. Android Port Any chance they might follow me in here? Any fool could guess that the
money had long since been spent. Anyhow, I'm quite unsure where to go from here. Anyways, I was planning to go through
the forest, to start with. Anyways, I'd better be off.
I'm trying to sort out this mess. Anyways, I'd better get back to work. Anyways, about the cave ahead...
it's just one guy, right? Anyways, if you must know,
I've actually come here to see Milgram. Anyways, not sure if you heard this part,
but I'm about to take a little trip. Are you done insulting me, yet? Are you quite alright? Are you really certain you want to do this?
I'm afraid I can't follow you into the caverns,
so this is the last you would see of me. Are you that green guy security is after? Argh! Who in the world would
design a house like this? Artist â¢ Characters, Monsters, Props, Terrain Artist â¢ Characters, Monsters, and Props Artist â¢ House interiors and exteriors Artist â¢ Props As you'd expect, stepping on them would hurt a bit. As you've played through this level,
you've probably noticed the coins. Asking one of the prisoners is probably
our best bet for a clue. At the store. Attack Awesome! Back Basement Junction Basic labor, mostly.
I haul construction materials around. Be careful about venturing into the caves!" Before we part ways, though,
one question: Besides the obvious cage situation. Besides which, you don't seem like a bad guy,
if you ask my opinion. Beware also, any bottomless pits you encounter.
Those are immediately fatal. Bit of a recluse, but he's quite sharp.
He lives on top of this rock column. Bleh. A locked door. Blocks Run Bombing Fools Bon Bosque Bound, bound, bound and rebound! Breakfast? ..bud, we're on lunch. Brute force against brute force won't work. Bug Fixer Bug Mining Bug Reports/Fixes Built it for exactly this sort of crisis. Burn and Bubble Burning Stone But I guess he really could cause
some issues if other people, like yourself,
aren't aware of his background. But hey - good luck.
I hope this works out for you. But if you're going to be like that
I think I left a health capsule down there.
If you can clear it out, it's yours. But it's not any crazier than
you transforming from a tadpole. But nothing ventured, nothing gained, you know?
I guess I'm just tired of bumming around. But that is how it works for frogs, right? But that's just CRAZY!
How does that even work?!  Magic? But that's not a secret area. But the town, you see ... But you might want to be extra careful
if you do sneak in. But, I'm not gonna turn down a good thing.
Thanks! But, apparently we just got conned. By all means, though,
you're welcome to wander around. CLIMB COIN RACE Can you give me any advice? Can't a guy sneak into his neighbor's
basement uninvited these days? Can't be worse than the crap I have
to deal with just coming into town, though. Casual Caverns of Holes Central House Central Services Certain bridges and stairways, like this one,
allow you to jump up and pass through them. Challenging Changed tilesets to make water look better. Chopple's General Store Cleaning.
I'm a janitor. Coin Race Collect 15 points cubes in a row. Collect a thousand coins. Collect ten coins. Collectible items like coins can be collected by being touched by the tongue, besides just frogatto's body. Complete redo of attack logic; frogatto now is free to walk around whilst he attacks, and in general is much more responsive. Complete redo of iOS controls, switching to a 2-button dpad from a 4-button.  Eliminates all problems with mispressed up or down keys interrupting other actions. Complete rewrite of damage handling; much more consistent now, and categorically eliminates some bugs where damage wouldn't get dealt. Complete the game within 1 hour. Congratulations, you just won the game! Controls... Could you just step aside? Could've been worse. Crevice Bridge Crevice Village Cube Chain Dark Corridor Darkness Central Descent to Town Devil's Advocacy Different enemies present different challenges,
and you'll need to get creative to deal with them. Directions Do Frogattoes live in caves? Do I like the ones that have an obvious point,
or is it more intellectually satisfying to puzzle
over the more engimatic ones? Do you ever feel like that? Do you guys get a lot of traffic? Do you help out here in the kitchen? Do you want to buy the ${consts.name}? Documentation Lead Does it look like I have a key for your cage? Does that sound like a good idea? Down the Rabbit Hall Downhill Dream on... Dungeon Blocks Dungeon Crawling Dungeon Gateway EXIT Each machine has a different arcade-style challenge. Eerie Arbor Eh, I'd hardly say they were 'scared' away,
but I might have tipped the scales. Eh, I've been better, but yeah, I'm okay. Engine Improvements Er... "made to", you say?
That's an interesting choice of words. Exactly! Except the elder. Excuse me whilst I vent a little frustration. Exit Game Fair enough.
I'm not particularly fond of it, myself. Fan House Fast Lane Features: Feel free to come back here,
if you want to read a hint again. Fetch some for our guest,
would you, kindly? Fight! Figures.
Well, thanks for the help! Finally, a chance to use your degree... Firstly, Milgram's got LOTS of followers.
Tons of em.  He runs an entire city. Fixed The Long Haul (arcade level) Fixed The Long Haul (compiled mode) Fixed a bug that caused a black line in the seaside background during dialogues. Fixed a bug that caused some tiles to incorrectly be flagged as opaque. Fixed auto-saving on iOS4 where multitasking is supported (iPhone). Fixed bug causing ambient sounds to not work. Fixed bug making certain music always play at full volume (iPhone). Fixed bug where a dialog could repeat forever. Fixed controlpad issues for computers. Fixed crash in world map when you hold down while entering it. Fixed crash when picking up max heart pieces. Fixed fullscreen mode (Mac/Windows). Fixed instructions on titlescreen Fixed instructions on titlescreen (iOS) Fixed lots of z-order issues. Fixed some crashing bugs. Flooded Caverns Foreboding Forest Fortunately, Frogatto can jump off of walls, too! Fountains, and other water dispensers,
are very helpful. Frogatto Grotto Frogatto is a classic-style 2d adventure game, starring a certain quixotic frog.  It's made by the same team that brought you Battle for Wesnoth, and features the same handcrafted love of high-end pixel art.  Frogatto is a side-on "platformer" or "jump and run" game, where you lead our titular character on a humdrum errand that turns into a daring adventure. Frogatto!
He's RIGHT HERE! Frogatto's House Frogatto's Room Frogatto's key mechanic is an unusual twist; like any frog, he jumps, swims, and snags bugs with his tongue, but unlike any frog before him, can spit those bugs out with deadly force.  Not merely an action game, frogatto is a full, classic adventure game, with characters to talk to, things to buy, puzzles to solve, and a wide, complex world to explore. Frogatto's tongue-grabbing is MUCH more forgiving Frogatto's world is a dangerous place. Frogourmet Further enhancements to dungeon wall graphics (added edges). Gaston should be coming out any minute now,
with a lovely bit of smoked salmon. Gee, guess who we'll find there? Gee, thanks. Geez, I'm sorry to hear that. Geosynchronous Orbit Achieved Get out of the way! Getting there.
It's gonna be a while, though. Glad I caught you!
You'd better be aware of this... Gloomy Glade Gnarled Knoll Go right ahead. God, I don't want to work today.
I just want to sleep. Golden Glen Good excuse to get out and travel a bit.
See the island. Good luck! Good luck. Got any advice? Grassy Path Grudge HEY, WAIT!
What about me?! Ha, well, we're also going to find out
if it's very foolish, as well. Hah! Handsomely you say?
I'm on it! Hanging Platforms Has he ever shown any signs of ... suddenly
having way more spending money than usual? Have fun! He asked me if I knew who it was. He chooses what we're shaped like, for one thing.
Usually takes inspiration from the animal kingdom. He does brag about being known by important
people, but you know how he is. He fancies himself an artist in this regard,
and for ego's sake I'm not about to disagree. He gives me money,
then he just 'decides' what I owe him. He may have used some of the money to build it,
but his house has some sort of safe room. He was going to invest a
sizeable amount of money, here. He was going to use this as an excuse
to take over the town. He's just plain nuts.
He's got a lot of bizarre delusions about things... He's not really a friend of mine,
but I'd like to know what's going on. He's one of my kitties.
Heads up one of our engineering divisions. He's such a miser, you know -
the man would probably burn his
furniture just to save on firewood. Hello there. Hello, again!
So ... what now? Hello? Here we are, after all, aren't we? Hey Nene, what's cooking? Hey guys! Hey there - is everything going alright? Hey! Hey! Over here! Hey,
I'm trying to help save the town. Hey, Frogatto. Hey, Hedge, you alright? Hey, how's breakfast coming? Hey, man, look at you! Hey, thanks, that's really nice of you! Hey, what's new? Hey.
Welcome to the store. Hi there, what's going on? Hi, Chopple. Hi, I'm Frogatto. Hi, again. Hi, what are you doing? Hidden Cave Hidden Depths Hm, I've never really thought about that before. Hmm, you know, I know one thing
that might help you.. Hmm... boy, where do I start? Hmmm... Honest trade and thrifty prices!" Honestly, it's been pretty funny to hear about.
Some of those security guys are total dicks. How can I tell who's friendly or not? How interesting. How is it that you're able to eat fruit? How much were you figuring on
paying for a pest exterminator? How's that business with Milgram going? However, they also hurt your enemies! I ... didn't ... use a vehicle.
I walked. I ... didn't see that coming. I ... see.
Uh... do you live here? I am? I better leave this key here. I can stop in at Nene's house
on the way to town! I can't decide which ones I like better. I can't really say. I confess, I really don't know -how-
he creates us,  but I know that he does. I do hope so.
Good luck! I don't care what you're preaching,
go sell it to someone else. I don't have to put up with this. I don't know where the controls would be,
though.  I'm not from here, after all. I don't know, but...
Why did this happen in the first place? I don't know; it's a little crazy to do this,
but I've kinda been itching for something
worthwhile to do. I don't see why he'd need an excuse.
If he wanted to take over the town,
he could just ... do it. I don't suppose you've had any
luck with getting a golden ant yet? I don't think I can promise anything,
but if the opportunity presents itself,
who knows? I don't think that would work. I don't think they were told
what they were looking for. I got tired of waiting,
and ate by myself. I guess I have to ask:
Whatever prompted you to build this? I guess I'm also worried about the other end;
what Milgram is doing, et cetera. I guess the only way out, is in! I hate this job.
It's CREEPY down here! I have a notion,
but if you're going to go up there,
you'd better go whilst the going's good. I have no idea what you're talking about. I have serious business to attend to. I haven't been there in a long time,
so I'm not sure what's become of it. I haven't given you a chance to apologize,
but no need.
Thank you for embarrassing them. I hear you had a big hand in scaring
those guys off.  Way to go! I hear you have those guys in a royal fit. I honestly have no involvement in
stuff like this; I delegate all of it out. I hope they've learned a lesson from this. I just think this is a big misunderstanding,
and I'd rather not see anyone get hurt. I know it sounds completely nuts,
but I honestly think that when he was given it... I know you're trying to hide something,
but throw me a line here. I know! I like you, little frog!
You're quite bold. I mean, even if we armed the whole town,
they'd grossly outnumber us. I mean, that seems pretty incredible
from my perspective. I mean, what are we gonna do,
sit on our butts here? I mean... on paper,
that sounds like a good enough plan, I need to go up... There must be
a way to activate this elevator. I need to take care
of something in town. I realized this was all a scam.
He was establishing a claim, you see. I say! Who's there? I see. I see... I set out for just a routine errand run,
you know? I suppose someone has to do the job. I sure do!
Cripes - I tell you what: I'll do it. I think I'm going to go back to sleep. I think the rank-and-file weren't
given very specific orders. I think they put out a security bulletin about you. I think they were just told
to secure the place. I thought so! I understand. I wanted to ask you about that incident in town. I wish you the best,
and I hope that helps keep you safe! I wouldn't mind running this by a few people,
since I have no idea what's true, or not. I wouldn't stand a chance, so I'm willing
to hinge my hopes on your being civil. I'd better be careful. I'd chat at length, but I'm a bit busy. I'd give about a 38%,
with a delta of about 15. I'd like to do something
to make amends with the town... I'll check back later, then. I'll go talk to Milgram,
and get this all sorted out.  Okay? I'll keep that in mind, thanks. I'm Frogatto. I'm afraid so. I'm as curious as you are, about this,
and we're more likely to get to the
bottom of things if we work together. I'm at a loss for a plan, myself. I'm doing laundry! I'm dreading the trip back. I'm just glad everyone's okay. I'm making a cake.
Would you like to have some? I'm not exactly sure what it will do, but
I'm told it has somewhat of a mind of its own. I'm not sure I follow you... I'm not sure I understand... I'm pretty tired, you see... I'm rather out of date on my political geography,
so you'll just have to ask people as you go. I'm reading a book. I'm trying to sneak in, because I hear he's
nigh-impossible to get a proper audience with. I'm whatever the occasion calls for. I'm working on it. I'm working on it.
Those things pack a pretty mean bite, you know. I've been thinking about that part,
and I have a hunch. I've never heard of any of this. I've really appreciated the "eyes in the sky".
It's very helpful. I've seen it one time, it's in the
house up and to your left from here;
up in the cliffside. If I answer yes,
are you going to call them in here? If I knew that, it wouldn't
be much of a secret, would it? If I may be so bold, I think it's
safe to say on behalf of the town... If I remember right,
one way to his castle will take you... hmm. If anyone asks, I didn't see you, okay? If they don't hit anything, they'll be stunned
momentarily, and then will flip upright again. If they were just looking for the elder
and didn't care about ticking people off,
it's one way to make sure he couldn't slip away. If you already know how to play,
then just walk to the next level. If you ask me, I think we need to go
hear Milgram's side of the story. If you bump into a wall at the crest of a jump,
you will cling to the wall and slowly slide down. If you don't get a running start, you can still
cling on, if you tap the arrow toward the wall,
and press jump whilst it's held down. If you don't mind my asking, where are we? If you drink from them,
you will restore all of your health! If you give the go-ahead, we'll be
happy to move on that next week. If you haven't already noticed, the red
fruits you sometimes find nestled in
the grass are spittable projectiles. If you hold down the down key while pressing
the attack button, you'll spit with very little force. If you hold down the up key while pressing
the attack button, you'll spit in a high arc. If you wanted a personal audience,
sneaking in is probably the best bet.
I doubt the entrance guards
would give you the time of day. If you're thinking to go in there,
you might reconsider. If you've seen a conversation before, it might
be annoying to have to read it again. Improved forest background, and made it go upwards infinitely. In fact, I came to him with the idea.
It was meant to be philanthropy. In his travels, you'll encounter
many creatures who want to hurt you. In the meantime, you might benefit from
visiting the fellow who lives here. Incredible. Initializing GUI Initializing custom object functions Initializing textures Initializing tiles Intense Intriguing.
That'd certainly explain a few things. Is everything alright? Is there any chance I...
can discuss something with you? Is there anything
you can do to help us? It has no special use, but hey - it's solid gold.
That can't be bad! It is indeed, my boy! It is.
It's just a crazy world we live in, huh? It makes sure everything works together. It says,
"Chopple's General Store,
just ahead. It says, "Attention:
Please clean up when you are done,
and no standing on the filing cabinet." It says, "Careful!
This area is extremely unfinished." It says, "Central Gate Control"
That's what we want! It says, "This leads to the North Tower." It says, "This leads to the cave entrance. It says, "This leads to the cellar and storerooms." It says, "This leads to the west tower,
and to the main throne room complex." It says, "Today's Special:
Caesar Salad and Avocado Wraps" It says, "Warning:
Transport to this chamber is one-way." It seems I'm trapped inside this house. It seems like some of the kitties are
packing up and leaving, actually. It sure is now!  Thanks for
taking those guys on! It turned out he'd hidden
in some sort of secret passage. It will take us several weeks,
and a ground crew to exploit it,
but it's probably worth the effort. It's always better that way;
gets money flowing around, makes the
new owners proud of what they built,
feels less like charity, et cetera. It's an important question,
and someone needs to look into it. It's controlled by this computer. It's easy to accidentally fall.
It's not really pedestrian-ready yet. It's fun to watch them get
a taste of their own medicine. It's important because it's much more
precise than anything we could do by hand. It's just a guess, but I honestly think he
might have hidden the rest right inside it. It's my very own safe room. It's often fine to just avoid them, but if you have
to fight, Frogatto's best defense is his tongue. It's possible you were. It's probably for the best, really,
I can't imagine the mess if people actually
got to fighting over stuff they disagreed about. It's the least I can do.
I'd offer to carry you, even,
but I'm afraid that's physically impossible. Jackalope Jeez, I *swear* this house
didn't look THIS big from the outside! Jump Just arresting everyone is a strange way
to do it, but I guess it's not so weird
if you've got lots of manpower to spare. Just gonna run by town, check the news. Keep an eye out for it! Key Remapping Code Kill 10 of the same type of enemy in a row. Kill 5 enemies in 10 seconds. Killer Bunnies Kitty Dormitory Know anything helpful
about this house? Knowing that, you could probably
just walk in and take it. LOAD GAME Lead Programmer Leading or spitting them onto harmful terrain
is an excellent way to dispatch them. Leftovers are in the fridge. Let's see here... uh...  Right.
Prospects in sector 29. Level Design and Object Scripting Level Designer â¢ forest, cave, and dungeon levels Level Designer â¢ seaside, town, and forest Like I said, I really don't know, myself. Loading Frogatto character Loading level Loading textures. Loading textures.. Loading textures... Lobby Lots of small graphical tweaks. Made golden ants less ambiguous. Made hearts heal a full heart, rather than just half. Made it so you can skip dialogs sequences. Made player status not show on the titlescreen or world map. Madness and wonder every step of the way. Maemo Port Maintenance Shaft Make this easy on yourself. Makes sense. Many small bug fixes. Maps are now strewn with spittable props within easy reach. Marvelous ventilation, though. Maybe I can talk my way around him? Maybe. Me?  In particular?
This is a tad awkward... Me?  No way I'm going to fight you.
You seem like a decent guy anyways. Meanwhile... Might've been something about a bridge,
Fergus was heading that one up. Milgram has this half of the town
locked down tight, and he has
most of the residents held prisoner. Milgram's Castle Milgram's Throne Room Milgram's Throne Room Entrance Milgram's crew.  When that captain-guy
came out of the house here,
they all hopped in his airship and left. Milgram's men showed up
early this morning, in town.
They've taken the whole place hostage! Milgram's trying to seal off the town,
so no one gets in or out. Minor Level Designer Minor Object Scripter and Level Designer Minor level changes. Moonlit Ramparts Music Volume: Musician NEW GAME Nah, I'd better get to town. Nah, this town doesn't have the fire for that.
Which is a shame in some ways, but ... Naturally, we know that didn't happen. Naturally, we wouldn't be having
this conversation if I hadn't gotten
the better of him. Nature Boy Nene's House Never. Nevertheless,
thank you very much for all your help! Nevertheless, Gaston is pretty nice
as far as bosses go. Nevertheless, I'd better be going.
Hopefully, we'll fix that bridge soon.
Impressive how fast they can do that,
with modular construction and all. Nevertheless, you can probably
figure that out simply by trying it. New translation: Italian New, more readable dialogue font. Next thing I know, people are SHOOTING at me!
I WANT TO KNOW WHY! Nice Try... No No kidding.  Well, hey, have a nice meal.
Thanks for all the info. No one in here works security,
so we won't come after you. No problem! No problem. No way!  Over my dead body! No!  You can't be... No, I didn't hear this. No, I'm not looking for trouble. No, no, don't get the wrong idea. No, no...
but I was hoping we'd go in in force. Nominally, I am the sous-chef,
but in practice I'm an errand-runner. Nope, it's locked. Nope.
Not until you answer my question! Nope.
You're on your own.  Good luck. Nor do I -
that's simply what I was told. North Courtyard Not Without My Hat! Not exactly. Not like I need to be up, Not that anyone's asking me. Not yet.
Sorry. Not. Interested.
Buzz off. Note that these have an unusual property;
if you use them up, walk away from the screen
where you found them, and then return again,
there will often be a new one waiting to use. Nothing to see in my backyard. Now applying artificial gravity. Now just up ahead there is a gate.
If you can get through it,
you'll make it to the limestone caverns.
There is said to be a way into the
basement floors of Milgram's compound,
somewhere in the caverns. Now what? Object Scripter Object Scripter â¢ Props and Achievements Of course not!  Robots don't eat.
We're all living creatures. Of course not.
That's just nutty. Of course.
I don't disappoint! Oh crap, oh crap!
Boss! He's here! Oh for crying out loud,
would you NOT play coy with me! Oh geez! I overslept! Oh hey, I hadn't seen you here before. Oh really?  Where at? Oh thank goodness, you can talk! Oh yeah, I forgot to mention;
he said it was some kind of "investment" Oh! I guess you wouldn't know these things...
I suppose we take that for granted. Oh, I was just sitting down for a bite to eat.
Care to join us? Oh, Milgram, of course.
Why else would he be so important? Oh, hello there.
I haven't met you before. Oh, hello.
I'm just eating some fruit. Oh, hey there!
Didn't see you coming. Oh, hi there! Oh, hi there.
I'm machining metal parts. Oh, hi, what's going on here? Oh, just cleaning up after that ruckus. Oh, no question. Oh, no you don't! Oh, so he's your parent, then?
And just has a lot of kids. Oh, well maybe before you go
you could look at my basement. Oh, what a shame. Oh? Okay ... I'm kind of at a loss for words.
That seems pretty unbelievable. Okay, I've had enough!  I'm going home. Okay, do you know what 'usury' is? Okay, then how's lunch coming? Okay, uhm, thanks. Okay.
What have I got to lose, eh? Okay.  Will you please lend
me that key? Okay... I guess that sorta makes sense,
although it's a bit convoluted. On this level, we give advice on how to play.
Each question mark has its own hint. Once spat out, they become a deadly projectile. One of Milgram's henchmen, no doubt. One or two. OpenFeint Ostensibly, I am a mole. Other small bug fixes. Ouch. Our Players Past that, and you come to the forest. Pato's Room Perhaps more importantly, I am a bit of
an astronomer and biologist, by trade. Phooey. Pinecone Park Plagued Mine Platform Peril Play! Playing the economy game, eh? Plus, they're pretty mobile, too.
Planes and such. Pocket Money Possible, but it's unlikely.
You can probably take a breather in here. Press ${DOWN_KEY} to crouch. Press ${JUMP_KEY} to jump. Press ${LEFT_KEY} or ${RIGHT_KEY} to walk. Press ${TONGUE_KEY} to use a character's attacks. Press ${UP_KEY} to talk, and use things like doors. Pressing jump while in the air pressing against a wall now makes you cling to the wall. Programmer â¢ Engine Development and Improvement Programmer â¢ Sound and iOS Code Programmer, Lead Content Wrangler and Lead Object Scripter Put a water beetle back in water. Quickly press ${LEFT_KEY} or ${RIGHT_KEY} twice to run. RUN Rampage! Really?
He's never talked about this? Really?
Where's your ship parked? Redid graphics for basement wall tiles Redid graphics for dungeon wall tiles Redid graphics for forest background Redid graphics for forest trees. Redrew the world map. Refactored damage and monster type systems to make behavior more consistent and eliminate some rare buggy behavior. Regarding THIS incident, I will need
to have a talk with them about common sense. Remarkable.
I really thought you'd come here to attack me. Residential Quarters.
It's really a dormitory-like arrangement. Resume Return to Titlescreen Reverse A and B Reviews: Reworked "darkness" for improved appearance Reworked some tilesets to eliminate tiling errors. Rewrote most of the dialogue. Risky business, though, isn't it? Rock-a-Fort Rocky Roots Roger! SELECT DIFFICULTY LEVEL: SPECIAL THANKS: Say ...
What happened to the elder? Scripting Language Designer Secluded Hut See to it. See, Milgram was being
what's called a 'loan shark'. See, some years ago,
Milgram approached me with a proposition. See, strictly speaking, we're not cats.
We're simply made to look like them. See, then he can just take
whatever he wants, as payment. Select Difficulty: Select Save Slot: Select a level: Select save slot to load. Select save slot to use. Show us what you can do! Significantly improved controls, both functionally and graphically (iPhone). Simple, just take to the air! So ... all's well that ends well? So I don't know what the elder talked you into,
but it's almost guaranteed to be lunacy. So I'm stuck here, huh? So all's well
that ends well, I suppose. So for a lack of any other willing parties,
I decided to go. So is this some kind of dining area? So let me get this straight... So nice to have things back to normal. So other than that, it's a mystery. So then he tells this oddball story
about how it's all some plot where
Milgram's gonna take over the town. So then... are you robots or something? So what is it you do for a, uh... job? So where was I? So you're made, you say...
Who, then, is your maker? So you're the one who's been causing
all this trouble, eh? So!  I've unlocked this gate, here. So, dare I ask:
What do we do now? So, how did it go? So, how did you get out of your cage? So, no, he's never seemed
to have a dime to spare. So, now what? So, what happened to lunch? So... uhm... loosely speaking, he says
that Milgram loaned him money, and this whole
incident was Milgram coming to shake him down. Some changes in elevation are too high
to clear with just a normal jump. Some hazards are like this.
You simply have to work around them. Some of them are kinda
thinly-veiled cautionary tales... Someday, those door-to-door people
will realize that kind of hard-sell tactic
just annoys people. Something's odd about this fireplace. Sometimes, you'll run into rather dangerous-
looking terrain, such as these brambles. Sorry, I'm off to town!
Not interested! Sound Effect Designer Sound Volume: Sounds like a perfect place for him! Speaking of which, I'd been meaning
to thank you for your help thus far. Speaking of which, am I imagining it
or is this house actually smaller now? Speaking of which, color me curious,
but I thought that cats were carnivores? Speed Trader Speedrun Splash, Hop Starlit Cave Start a level with no health left. Sticking out the tongue and spitting,
are both done with the attack key. Stonepipe Bridge Stonepipe Meadow Storerooms Strange.. Suboptimal Design Such a beautiful view from here. Suddenly their motives don't seem
so cut and dry. Sure thing. Sure... Take your time, look around. Talk it over.  See if we can reach
some mutually amicable agreement. Talk it over? Like gentlemen? Technical Advice Tell me about it. Thank you Jeeves, that will do nicely. Thanks - I'm kind of in a hurry,
but I appreciate it. That ... uh, "captain" cat, or whoever he was,
tried to dice me up. That I owe him more money
than I could ever repay. That certainly...
changes my plans for the day. That guy sure doesn't look glad to meet me! That is a distinct possibility. That is why I live all the way up here.
You can see quite a bit from this height. That makes sense. That might have been why
he tried to take out that bridge. That old clown was just sabre-rattling
about raising a militia. That seems like an enormous waste of effort. That sounds like a pretty good system. That sounds pretty classy. That was really brave of you! That's actually a rather clever
way to round everyone up. That's good to hear! That's it for this tutorial. That's news to me!
Where did you see that? That's nice to hear. That's nice. That's nice.
What's it about? That's nutty. That's really amazing.
Uh... That's right! That's right.  Unfortunately,
my collection lacks the rarer golden ant. That's the infinite cage. That's very interesting... That's very noble of you. The Cat-Factory's Catwalks The Danger House The Elder's House The Elevator House The End The Entrance The Kitty Factory The Long Haul The North Tower The cave that leads into the town,
has a sentry posted in it. The elder's house is off to the east,
past the town gate. The energy crystal unlocks
energy powerups. Energy
powerups will allow you
to shoot bolts of energy. The energy shield protects you
from damage when you crouch. The floorplan is quite loony. The furthest that has been reached is level ${registry.arcade_coin_race_high_score}. The game has been saved. The gliding wings will allow
you to glide through the air. The heart will give you
an extra health point. The high score is ${registry.arcade_blocks_high_score}. The high score is ${registry.arcade_down_the_rabbit_hall_high_score} points. The high score is ${registry.arcade_maintenance_shaft_high_score} points. The high score is ${registry.arcade_nutty_factory_high_score} points. The high score is ${registry.arcade_the_long_haul_high_score} points. The high score is ${registry.arcade_upper_deck_high_score} points. The high score is ${registry.arcade_wild_new_wonder_high_score} points. The holes and such are all standard,
consistent sizes, which means that
they fit standard-sized bolts. The hyper crystal unlocks
hyper powerups. Hyper
powerups will heighten your
reflexes, making the world
seem to slow down around you. The intruder?
How did he get in here? The invincibility crystal unlocks
invincibility powerups. Invincibility
powerups will make you
invincible for a short time. The job is a bit of a pressure-cooker,
no pun intended, but he doesn't
use that as an excuse to be mean. The power boots will
make you jump higher. The price is ${final_price} coins. The sooner, the better. The speed boots will make
you faster. The spit strengthener will extend
the range you can spit. The tongue extender will extend
the length of your tongue. Then I suppose that Milgram's guards saw you
and locked you up? Then again, I'm almost tempted to cut my
losses and leave you guys well enough alone. Then again, the world is nothing
if not full of surprises. There are a few ants just ahead.
Feel free to try this on them. There is a store conveniently located
outside of the town. There is no way to stop this;
you can't destroy the anthill. There must be a way around it. There's Nene's house, right up there! There's got to be something
we can use to our advantage,
but "numerical superiority" isn't it. There's no fooling me. There's supposedly
a secret room in this house. These are an ample source of ammunition,
that's almost always available. These crazy bombers are everywhere! These days, moreover, we're mass-produced.
There's a factory downstairs that churns out
the more successful designs. These have excellent range and decent damage,
but are somewhat draining to use. They may have seen me talking to you,
I can't be sure. They're tied up inside the houses,
under guard. If you free them,
I'm sure they can help you. They've probably also turned-on
our security systems, unfortunately. This has been a really crazy day,
and I'm fresh out of patience. This has excellent range,
and does decent damage. This is a new high score! This is a surprise? This is a very deep chasm... This is decidedly unpleasant. This is especially useful, if two walls are
facing each other, because then you can
rebound over and over between them. This is true. This lets you ascend vertical columns. This thing's kind of a pain to program,
but it's a small price to pay. Through there,
is the way to where Milgram lives. To Nene's House To do so, hold down the down key,
and then press jump. To do this, do a walking jump into the wall. To find out more about the game please visit http://www.frogatto.com/ To my knowledge, yes. To save your progress in the game,
use one of these convenient restrooms. To spare your asking,
yes, I'd be happy to continue. Tongue Translation Code Translation Coordinator Try to eat all kinds of enemies. Trying to get his money back. Twisted Trees Ugh!
You do put up quite a fight! Ugh, I suppose you're right. Ugh. Ugh.
No sense in turning back now, though. Uh ... why would that happen? Uh, maybe...
I certainly appreciate the hospitality! Uh, oh, I hear ... trouble. Uh, this'll take you over a bridge,
then through an underground passage. Uh... Uh...
That's a pretty spectacular euphemism, there... Uh... hi? Uhh, okay...
Anyways, what are we gonna do here? Uhh..
I was expecting something a bit more ... robust. Uhm...  so, I'm not even sure what that
project was supposed to be, at this point. Uhm... well, don't you have any maps or such?
Are you going to help outfit me for this? Uhm... yes? Underground Pool Unfair Unfortunately I don't.
I was just visiting. Up ahead, you'll notice an anthill,
which ants constantly come out of. Updated music. Upper Deck User Interface Code Version 1.0.1 (iPhone only): Version 1.0.2: Version 1.0.3: Version 1.1.1: Version 1.1: Version 1.2: Very well. Victory! WHOA! Wait a minute, it's you, isn't it? Wait,
did he think you were actually the mayor? Wait, what?  Milgram? Walk in front of a door or person, and press Wandering Passage Was I asleep? Watch your head in there. Watery Alley Way upstairs there's a control room
with a big button to open it. We also know Milgram's not interested
in taking over the town, because he
essentially just did, and gave it up. We can do that. We do.  ... I don't mean to be rude,
but I think I'd better get back to work. We don't know if the money thing
is true, but we know if it is,
he certainly hasn't put it to use. We get that from time to time. We know he's clearly run afoul
of Milgram, somehow. We may.
In fact I believe we will! We might get the drop on them for a bit,
but we'd get surrounded in no time. We need some help around here. We saw Milgram's lieutenant go in there. We should keep in touch - hopefully,
through you, I can meet some of the
more 'level-headed' members of your town. We thought we'd actually hire you guys
as labor, to do whatever you could. We'd be like ... 
a bull butting heads with a freight train. We're called 'Frogs', actually...
I guess sometimes we do, but I don't. We're still working on the north tower.
Be careful if you venture out there. We're twins. We've all known him for years,
and he's a hoarder, and a miser. We've never seen him suddenly having more
money to spend; so it's crazy, but it fits. We've trumped him up with a pointless job
as the gatekeeper on the east side of town. We... We tried to stop him, but he's... Webmaster Weird.
I wonder what they want with the elder? Weird.. that doesn't make any sense!
Why would they just let you go? Well sheesh, I thought you'd
do it for free for a friend! Well!
That went better than expected. Well, I am the mayor, after all,
and mayors are well endowed. Well, I can certainly at least
guarantee transportation for you! Well, I can't let anyone
past this gate. Well, I guess we consider him a parent,
but we're very much made. Well, I think we should form a militia,
and bring the fight to Milgram! Well, as you know, the town
is split into two halves. Well, dangit, this door is locked. Well, good luck!
I'll be around. Well, hey - you be careful, alright?
Stay cool. Well, if they're armed,
assume they're with security. Well, if you really are here for an audience,
perhaps I could be obliged.  Shall we? Well, if you say so, I suppose I can't object.
In fact, it might be a good stalling tactic. Well, if you're game to find out,
I just saw one of Milgram's lieutenants
march up to the elder's house. Well, is that really who they were after? Well, pretty soon, I hope.
Sometimes he takes a while. Well, thanks, I do appreciate that. Well, that settles things for me.
You two have fun. Well, the fact that I'm unarmed
is a pretty big factor. Well, they just let me out. Well, this machine here is a router.
It drills holes or grinds out shapes in metal pieces. Well, we could use a bit more help. Well, whatever you did,
I'm glad they're gone! Well, yeah,
I was gonna go in there. Well, you made it! What are YOU doing?
For that matter, who are you, anyways? What are we planning to do about
the rest of Milgram's army? What are you even doing? What business do you have with him? What did the elder actually tell you? What do you mean? What does that mean? What the... What was it you wanted? What you're saying, is that the elder
borrowed all this money from you, What's important is that I'm not in a cage,
so I might be able to do something to help. What's our next order of business? What's wrong?! What?!
They ... as in Milgram's kitties? When I stepped in the door,
it locked behind me. When he was getting shaken down by that kitty,
we turned our back on him for a second. Where's your sibling, by the way? Which I assume would be for the town.
Building things. Which plays to his ego,
and keeps him out of trouble. Which sometimes doesn't work out as expected? Whilst on their backs,
they're vulnerable to being stomped on. Whilst you are clinging to the wall,
pressing jump will make you rebound off. Who are you? Who knows?
Maybe something good will come of it? Who the hell are you? Who's here? Whoa...
... is that what I think it is? Why do I have to deal with everything myself?! Why don't we start with:
Why was that guy asking about money? Why, I'll even show you the way... Wild New Wonder Will do.  Catch ya around. Will you hear me out now? Win! Windows Packager Without this key,
you're not getting anywhere. Woah! A flying plant! World Map Worse comes to worse,
I can just run for it.  Right? Would that someone possibly be you? Would you have any suggestions? Wow, so they seriously just packed up and left?
That's amazing! Wow, uhm... 
I'd sure like to help, but ...
I just don't see what I could do. Wow, you guys look exactly alike. Wow. Wow.
That's pretty bold! Writer â¢ Wrote Main Story and Most Dialogue YES! Yeah! And to think that I thought
that the day was going to be boring! Yeah, I couldn't help but overhear, and
... good news: Your problem is solved! Yeah, I just hope we don't
have a repeat incident... Yeah, I know.  I'll try. Yeah, I thought you were some creature
that had shambled in from the caves. Yeah, I'll check it out. Yeah, I'm a little worried about
what might happen if someone doesn't
get in touch with Milgram. Yeah, it isn't bad. Yeah, no kidding.
Well, see you soon!  Thanks again! Yeah, one of their head guys showed me
a drawing of him. Yeah, small perks like that make life nice. Yeah, that doesn't surprise me. Yeah, that key right next to you -
I think that unlocks a house that
has the controls to the town gate. Yeah, this is one of our dormitory areas.
Quite a few of us live here. Yeah, this whole day has just been ... bizarre. Yeah, those goons
had a little ambush set up. Yeah. Yeah.
Sad to say, I can't pick locks.
I got nothing. Yeah.
Who WAS that guy?! Yeah.  Geez.
So, uh, here's what I think we know: Yeah.  I don't know a lot about Crevice Village,
but if you'll forgive me, I had the impression
it was a bit of a backwater, so I thought
we'd do something to help out. Yeah... geez though.  Caves, now?
Are these caves long? Yep. Yes Yes, I am; I'd be twice the fool,
to come this far and just give up. Yes, I do think everyone will have
an equitable outcome to this whole mess. Yes, I've been getting some rather
unpleasant reports about property damage. Yes, I've pulled a few strings,
called in a few favors and whatnot... Yes, he's our official 'greeter'. Yes, here's the key.
There're some ants down there. Yes, hi. Yes, in fact I do. Yes, it is.  We have regular meal service
for those who are off-duty. Yes, it's unreasonable to expect
someone to just charge in there. Yes, like gentlemen. Over tea. Yes, okay ...
they were just thugs, plain and simple. Yes, that's right - I'm implying we are "made".
We're made, not born, like you ... presumably are. Yes, well, at this particular juncture,
who I am isn't very important. Yes. Yes.
Yes it was. Yes... it is the only path I know of,
but be careful in there. Yes... oh dear.
I see what you mean. You bet I am. You bet. You can also jump down through them. You can collect these
and then use them to buy stuff! You can grab enemies with the tongue,
and then spit them back out. You can now fire balls of harmful
energy from your fingertips,
which home in on your enemies. You can now fire balls of harmful
energy from your fingertips. You can skip a conversation, including parts
of cutscenes, by pressing the "escape" key. You don't have enough money. You don't say. You don't think he could actually
pull that off, do you? You found a golden trophy! You gained a new ability! You got ${level.player.score} points in the Blocks Run! You got ${level.player.vars.score} points going down the rabbit hall! You got ${level.player.vars.score} points in the Nutty Factory! You got ${level.player.vars.score} points in the Wild New Wonder! You got ${level.player.vars.score} points in the long haul! You got ${level.player.vars.score} points in the maintenance shaft! You got ${level.player.vars.score} points on the Upper Deck! You guys ... kinda scared a few people. You have acquired a heart capsule.
Your maximum HP has increased by one! You know, I'm not gonna lie - it's really weird
to fight you guys in one place and ... uh You know, now that you mention it,
you look kinda familiar. You look like a very
suspicious character. You made it to level ${level.player.difficulty} of the coin race. You must excuse me, but I'm terribly busy.
I don't have any time to speak right now. You need to get up earlier. You probably should hear what others are
interested in, but I for one really wish there
was a road of any kind between here and there. You should also beware that not all enemies
can be grabbed with the tongue. You still haven't heard, then. You!! You'll find many such objects of similar size
in different areas of the game, such as crates
inside of houses, or nuts hanging from trees. You're an intruder, aren't you?! You're welcome to sit down and join me.
The fire is quite nice. You're welcome, but it's a tad premature...
I have something for you which I hope will help. You're welcome. You've collected enough parts to
make a whole new heart capsule!
Your maximum HP has increased by one! You've found part of a heart capsule.
Collect another one to increase
your maximum HP by one! You've found part of a heart capsule.
Collect another three to increase
your maximum HP by one! You've found part of a heart capsule.
Collect another two to increase
your maximum HP by one! Your basement? Your big problem will be that
the gate connecting the two halves
of town has been locked. Yup, I'm Frogatto! ^Yeah. and it was meant to be used on
civil projects for the town? but I'm gonna be a jerk here,
and poke some holes in it. but it's the principle of the thing. empty energy crystal gliding wings h... Hello? heart hyper crystal invincibility crystal keywords: platformer,pixelart,oldschool,platform,game,pixel,arcade,action,adventure,frog spit strengthener the button that appears to interact with them. tongue extender Project-Id-Version: Frogatto
Report-Msgid-Bugs-To: http://code.google.com/p/frogatto/issues/list
POT-Creation-Date: 2012-11-28 22:10-0200
PO-Revision-Date: 2012-11-29 00:12+0000
Last-Translator: Marcos Avila Isidoro <marcavis@yahoo.com.br>
Language-Team: Chinese (China) (http://www.transifex.com/projects/p/Frogatto/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 âæ­¥è¡å±é©ï¼èå·¥ä¸ç¨ï¼â âæªç»ææé²äººåè¿â âå·¥äºï¼æ´å®å¨å¸½â ï¼åæ¶ï¼ - OpenFeintåæå°±æ¯æ - [3/4 SlideToPlay.com] "a fun and surprisingly deep platformer" - [4/5 AppSpy.com] "boy-howdy is this an amazing visual feast!" - [n/a TouchArcade.com] "totally worth checking out." - è¡æºæ¨¡å¼ï¼ç«å³è¿å¥éæºæ¸¸æå³å¡æ´»å¨ - è´­ä¹°è½å - ä¸ç¨æå¿ä¸¢å¤±è¿åº¦ - æä½æäººç¨èå¤´ååºè¿è¡è¿«å®³ - å¤§éé³è½¨ï¼è¶è¿ä¸å°æ¶ååé³ä¹ - æ¬¢ä¹çå¯¹è¯ â¦â¦ â¦â¦
å¥½å§ã ...
è¿åæç¹è®©æä¸ä¹ æ¯äºã â¦è¿ä¸ä¼åå¨è¿éå¹³å¸¸çäº¤è°ï¼
æè¿æ¯èç­è½¬ä¸è¿æ¥ã
æ¯æä¹ä¸åäºï¼ ... å¤§å¤é½æ³è¦æ¥æ¬ºè´ä½ ã â¦å¥½æ­¹è¿ä»¶äºè§£å³äºã ... é£äºç«æ²¡ç¹å¿ç¤¼è²ã ...ç¶åï¼ ...æ±æ­ â¦â¦æçä½ æè¿äºèèå³å¨
ç¬¼å­éæ¯ä¸ºäºæç ç©¶ï¼ â¦æ¿å°äºè¿ä¸ªåéæ³å®ã
ç»ä½ ç¨ã ...å¤æ°æ¶åæä»¬åä»ç©ç¬ï¼
å ä¸ºæ å¦¨å¤§éã ... ä½æ¯ä¸»é¢åä¸º:
"å¥ä¾µèéå°ä»ä¹ã" ... ä»æ²¡æä¸»æå¦ä½å»åãåä»ä¹
æä»¥ä»...ç®åèç´æ¥å°èèµ·æ¥äºã ...å¶ä»äººè¯å®å¾å´å¥è¿ç§å¢ä½åä½é¡¹ç®ã æ¼«é¿çå¡æèµ°å» å¯ä»¥ææ°ä¸ä¸ä¹ï¼åçï¼ å¾é«å´è§å°ä½ ã
ææ¯æ©å¡è«ã å½è¿å¯è½çæ¶åï¼
ä¸ä¸ªä¸é¨çæé®å°±ä¼åºç°ã ç®åçå½é¢ï¼ä½ è½æ§å¶å·åºçæ¹åã åååååï¼æä¸ä¼é£åï¼ è¡æºæ¨¡å¼ å¤è°¢äºï¼ æå°±è¾¾æï¼ å®éä¸ï¼ç®äº - ææ­£å¨è°æ¥å®ä»¬çå¨æºã æ·»å äºå¤ä»¶å®¤åéå· æ·»å å ç§æ°æªç©ï¼å¥½åçº¢è²ç²è«ï¼èè çä¸¾å¨ä¹åäºã æ·»å äºè¡æºæ¨¡å¼ Windowsä¸æ·»å èªå¨è¯­è¨éæ© æ·»å èªå®ä¹æ§å¶ã æ·»å åé¢å¿ æåç»é¢æ·»å äºé³ä¹åé³æçé³éè°è æ·»å äºæ£®æåæµ·è¾¹å°åºçæ°è°è²æ¿ æ·»å äºé¢ åAä¸Bæé®çéé¡¹(iPhone)ã æ·»å  scons èæ¬ç¼è¯ã æ·»å å ç§æ£®æéç¬ç¹çæäºº å¨å³å¡éæ·»å âé»æâæ¯æ æ·»å  WVGA æ¯æ æ·»å éå¤å³å¡æ¯æï¼å¯¹åºè¡æºæ¨¡å¼ï¼ æ·»å 4ç§å®æ´è¯­è¨ç¿»è¯æ¯æï¼æ³è¯­ãè¡èçè¯­-å·´è¥¿ãæ±è¯­ï¼ç®ä½ï¼åæ¥æ¬è¯­ã å¯¹ä»ç±³æ ¼æå§ï¼ççï¼ åï¼æç¥éäºâ¦â¦
é£æ¨ç»§ç»­å§ã è¿æ ·ãæå»äºï¼å³å®ä¸ï¼æ°ä¸»è¿æ¯å¥½ä¸»æã åï¼æä»¬å°äºâ¦â¦ é¢ï¼æ¯å§ï¼æå¸®å©å·§æ¯ç»è¥ååºã
ä»è¦å»éå­ççåççäºæï¼æ¯ç«æ´ç©´ä¸é¢ä¼ æ¥
é£ä¹å·¨å¤§çåªé³ã åï¼å¾å¥½ï¼ä½ å®å¥½æ æå°
éè¿äºï¼ åï¼æ¯ä¸æ¬å¤èç«¥è¯éã åï¼ä½ å¨é£å¿ï¼ æ¯æ¶æ¯å»ã é½æ¯ä»çéã èå¤´å¯ä»¥ç²åç©ä»¶ï¼æ¯å¦é¥åã
è¿æ ·ä½ è½å¸¦çå®ä»¬å»å¼éã å°½ç®¡è¿ææ´å¤ï¼ä¸è¿ï¼è¿æ¯ä½ èªå·±æ¢ç´¢å§ã å¾å¥½...å¦æä½ ä¸å¨æï¼ææå ä¸ªé®é¢ã ä¼å»ï¼ èä¸æä¸è®¤ä¸ºé¨è¾¹ä¸ä¼æä¸ªå®å«
ç­çä½ ä»ä»èº«ä¸å·èµ°é¥åã é£æå¾ä¸å»â¦â¦æä¹ä¸ï¼ ææºé«å´è¿æ ·çè®¡åï¼æå¯ä»¥ä»é±ç»ä½ å½æ
ä»¬çéä¿¡äººã ä¸ç¨æå¿ââè¦æ¯ä½ æäºï¼
ä¼å¨è¿ééæ°å¼å§ã å¦å¤ä¸ç¨æå¿ï¼
æä»¬è¿éæ²¡æåä¿å®çã æ ¹æ®æåæå¬å°çè°è¯ï¼
æç¥éè¿è·ä½ æå³ã ç°å¨ä»è¿æç»è¿ä½ é±ï¼ ä¸æ¦ä»ä»¬åè§ï¼æä»¬çä¿å«äººåä¹äºåæ¶è´¢
ç©ã é¡ºä¾¿è¯´ï¼è°¢è°¢ä½ çç¤¼ç©ã é£ä»ä¹ï¼ å¹¶ä¸ï¼åæ ·é®å·ä¹ä¼ç¨ä¸å®ã Android ç§»æ¤ ä»ä»¬æå¯è½è·å°è¿éæ¥åï¼ ç¬¨èä¹ä¼çå°è¿äºé±æ©å°±è±ä¸äºã å¦ä½ï¼æå®å¨ä¸ç¥éä»è¿éå»å¾åªå¿ã ä¸è¿ï¼æåå¤ä»æ£®æè¿å¥ã ä¸ç®¡åè¯´ï¼æè¿æ¯å¾åºåäºã
æå¾æè¿çæå­æå®äºã å¥½ï¼æå¾ç»§ç»­å·¥ä½äºã ä¸ç®¡æä¹è¯´ï¼å³äºåé¢çæ´ç©´â¦â¦
åªæä¸ä¸ªäººå®çï¼å¯¹ä¸å¯¹ï¼ å¦æä½ ä¸å®è¦é®çè¯ï¼
æå¶å®æ¯æ¥è§ç±³æ ¼æå§çã è¿æï¼ä½ å¯è½å¬è¯´è¿ï¼æè¦å»æè¡ä¸éµå­ã å¯¹æçéªæ°è¿ä¸å¤ä¹ï¼ ä½ è¿å¥½åï¼ ä½ ççè¦å»åï¼
æä¸è½è¦è·ä½ è¿æ´ï¼
è¿æ¯ä½ æåä¸æ¬¡è§æã ç»¿è²ä¿å®æ­£å¨æ¾çæ¯ä½ åï¼ åï¼è¿ä¸ä¸æåªä¸ªå®¶ä¼
ä¼è®¾è®¡åºè¿ç§å±å­æ¥åï¼ èºæ¯å®¶â¢è§è²ï¼æªç©ï¼éå·ï¼å°å½¢ èºæ¯å®¶â¢è§è²ï¼æªç©ï¼éå· èºæ¯å®¶â¢å»ºç­å®¤ååå®¤å¤ èºæ¯å®¶â¢éå· å¦ä½ ææï¼è¸©ä¸åä¼¤ã éè¿æ¬åºæ¯çæ¶åï¼
ä½ ä¹è®¸å·²ç»æ³¨æå°äºé£äºéå¸ã å»é®é®äººè´¨ä¹è®¸æ¯æä»¬è·å¾çº¿ç´¢çæå¥½æ¹
æ³ã å¨ååºéã æ»å» å¤ªå«å¥å¹´äºã è¿å å°çªè¿æ¥ç¹ åºæ¬çå³å·¥ã
æè´è´£æ¬è¿å»ºè®¾ææã æ³¨æä¸è¦è¿å¥æ´ç©´ï¼â å¨æä»¬å²å¼ä¹åï¼ä¸ä¸ªé®é¢ï¼ å¾ææ¾æç»å³ç¬¼å­éäºï¼
é¤æ­¤ä¹å¤è¿ä¸éã èä¸ï¼ä½ å¼èµ·æ¥ä¸æ³åäººï¼
ä»¥æçç¼åæ¥çã æ³¨æï¼æ åºæ´æ¯ä¸ä¸æ¥çãä¸è¦å»é¯ã ä»ç®åä¸ªéå£«ï¼ä¸è¿åæ¯å¾å°å»ã
ä»ä½å¨è¿ç³å´çä¸é¢ã åï¼é¨éä½äºã æ¹åè· çç ´ç¬¨è ç»åæ ä¸ å¼¹ï¼å¼¹ï¼å¼¹ï¼åå¼¹ï¼ æ©é¥­ï¼â¦â¦èåï¼æä»¬è¯¥ååé¥­äºã ç¡¬ç¢°ç¡¬æ»åäºã Bug ä¿®å¤äººå è«è«å¤§æ¶é Bugæ¥å/ä¿®æ­£ æ­£æ¯ä¸ºäºè¿ç§å±æºèå»ºçã ç«ç°ä¸æ°´æ³¡ ç¼ç§ä¹å²© ä»ä¹å¯è½ç»å¶ä»äººé æéº»ç¦ï¼æ¯å¦ä½ ï¼
å ä¸ºä½ ä»¬é½ä¸ç¥éä»çèæ¯ã ä¸è¿ï¼ç¥ä½ å¥½è¿ã
å¸æä½ è½è§£å³ä½ çé®é¢ã ä½å¦æä½ éè¦ä¸å¯ï¼
æè®°å¾å¨é£ä¸é¢çäºä¸ä¸ªçå½è¶åã
è¦æ¯æèèæ¸çå¹²åï¼é£è¶åå°±æ¯ä½ çäºã ä½æ¯æ¯èµ·ä»èèªååæä½ ï¼
ä¹æ²¡æå¤èè°¬å§ã ä¸è¿æ ç§æ æ¶ï¼ä½ ç¥éåï¼æåªæ¯é²å¤äºã ä½æ¯éèå°±æ¯è¿æ ·ï¼å¯¹å§ï¼ ä½æ¯è¿å¤ªèè°¬äºï¼
è¿æä¹å¯è½ï¼ï¼é­æ³ï¼ ä¸è¿è¿å¯ä¸æ¯å¥ç§å¯å°ç¹ã ä½æ¯éä¸â¦â¦ä½ ç¥éâ¦â¦ ä½æ¯ä½ è¦æ³æ½å¥çè¯
æå¥½å¤å å°å¿ã ä½æ¯ï¼æä¸ä¼æç»æ¯ä¸ä»¶å¥½äºã
è°¢è°¢ï¼ ä½æ¯ï¼æ¾ç¶æä»¬æ æ³äºè§£ã ä¸è¿ï¼è¯·éä¾¿ééã æç¬ ç¡¬å¸ééèµ ä½ è½ç»æäºå¿ åä¸ï¼ ç°å¦ä»å·²ç»ä¸å¯è½ä¸è¯·èªæ¥
æææºè¿é»å±å®¶çå°ä¸å®¤äºä¹ï¼ è¿è½æ¯éä¸çæ¯æåµç³ï¼ æ¾æ¾ æ´ä¹æ´ç©´ ä¸­å¤®å±å­ ä¸­å¤®æå¡ æäºæ¡¥æ¢åæ¥¼éï¼åè¿ä¸ªå¯ä»¥è·³ä¸åç©¿
è¿ã æææ§ æ¹åå¾åè®©æ°´çæææ´å¥½ å·§æ¯çä¸è½ååº ææ«ãææ¯æ¸æ´å·¥ã ç¡¬å¸ééèµ è¿ç»­æ¶é15ç¹æ°çæ¹åã æ¶éä¸åæç¡¬å¸ã æ¶éåæç¡¬å¸ã æ¶éç©åå¯ä»¥ä½¿ç¨èå¤´ å¨æ°æ»å»æ¹å¼ï¼é¿åç°å¨å¯ä»¥çå¿«å°èªç±ç§»å¨ï¼æ»å»æ¶ä¹ä¸ä¾å¤ã iOS æ§å¶çéæ°å¶ä½ï¼æ¹ä¸º2é®ææ§ãé©±é¤å¨é¨è¯¯æä¸ä¸é®å¹²æ°å¶ä»å¨ä½çè¡ä¸ºã éåä¼¤å®³å³ç³»ï¼æ´å åè¡¡ï¼ä»¥ååæ¶äºæäºæ æ³é æä¼¤å®³çbug ä¸å°æ¶åå®ææ¸¸æã æ­åæ­åï¼ä½ æéäºæ¸¸æï¼ æ§å¶é®... è½ä¸è½è¯·ä½ è®©ä¸è¾¹å°±å¥½ï¼ éº»ç¦ä¸æ­ã è£å£æ¡¥ è£å£æ è¿ç¯æ¹å é»æé¿å» æé»ä¸­å¿ è½å¥å°é é­é¬¼ä»£è¨äºº ä¸åæäººæä¸åçç­ç¥ï¼èªå·±å¯»é¨è·¯ã æ¹å é¿åç±»æ¯å¨æ´ç©´ä½çåï¼ æåæ¬¢æææ¾çæä¹çæäºï¼
è¿æ¯ä¸ºç¥ç§çæäºè¦å¿æç´¢
æ´æä¾ç¥æ§çæ»¡è¶³ï¼ ä½ æè¿æ ·çæè§åï¼ ä½ ä»¬è¿éçæå¿åï¼ ä½ æ¯å¨å¨æ¿å¸®å¿çåï¼ æ³è¦ä¹° ${consts.name} åï¼ ææ¡£ç®¡çå çèµ·æ¥æä¼æä½ çç¬¼å­çé¥åï¼ å¬èµ·æ¥ä¸éå§ï¼ æ·±å¥ååå¤§å ä¸å¡ åç¾æ¢¦å§â¦ å°ç¢åºå æªè¿å°ç¢ å°ç¢åºå¥å£ éåº æ¯å°æºå¨ä¸­é½æä¸ä¸ªä¸åçè¡æºé£æ ¼çææå³å¡ã é´æ£®æ æ å¦ï¼æå¯ä¸ä¼è¯´æä»ä»¬âåè·äºâï¼
ä½æ¯ä¹è®¸ç»ä»ä»¬äºä¸ç¹å°çæè®­ã åï¼ä¹åå¯æ¯è¿å¢åµå¥½å¤äºï¼ä¸è¿å¢ï¼
æ¯äºï¼ææºå¥½çã å¼ææ¹è¿ åâ¦ä½ è¯´âå¶é âï¼
é£æ¯ä¸ªæè¶£çè¯´æ³ã æ­£æ¯å¦æ­¤ï¼ é¤äºé£èå®¶ä¼ã æ±æ­æå¾å®£æ³ä¸ä¸æ«ææã éåºæ¸¸æ æ²¡é®é¢ã
ææ¬äººä¹ä¸æ¯ç¹å«åæ¬¢å®ã é£æä¹å± æ·å¾ ç¹ç¹ å¦æä½ æ³è¦éè¯»ä»»ä½ä¸æ¡æç¤ºçè¯ï¼
éæ¶é½å¯ä»¥åå°è¿éã è½ä¸è½éº»ç¦ä½ ï¼
ä¸ºæä»¬çå®¢äººåä¸äºæ¥ï¼ å¼æï¼ äºè§£äºã
æ©ï¼è°¢è°¢ä½ å¸®å¿ï¼ ç»äºå°äºç¨ä½ çè ¢ä¸»æçæ¶å... é¦åï¼ç±³æ ¼æå§æ*å¾å¤*æä¸ï¼
æåä¸ä¸ãæ´ä¸ªåå¸é½æ¯ä»ç®¡çã ä¿®å¤äºé¿æ¶é´åæ»é®é¢ï¼è¡æºæ¨¡å¼ï¼ ä¿®æ­£äºé¿æ¶é´åæ»ï¼ç¼è¯æ¨¡å¼ï¼ ä¿®å¤äºä¸ä¸ªbugï¼å¨æµ·è¾¹èæ¯çå¯¹è¯æ¡ä¸­åºç°é»çº¿çæåµ ä¿®å¤äºä¸ä¸ªbugï¼æäºå¾åè¢«éè¯¯çæ è®°ä¸ºä¸éæ æ¹æ­£äºæ¯æå¤ä»»å¡çiOS4ä¸èªå¨ä¿å­çé®é¢(iPhone)ã ä¿®å¤äºé æèæ¯é³æä¸å·¥ä½çbug æ¹æ­£äºä¸ªå«é³ä¹æ»æ¯ä»¥æ»¡é³éæ­æ¾çéè¯¯(iPhone)ã ä¿®å¤äºä¸ä¸ªå¯¼è´å¯¹è¯æ ééå¤çbug ä¿®æ­£äºè®¡ç®æºæ§å¶æ¿çé®é¢ã ä¿®æ­£äºä¸çå°å¾çå´©æºï¼å½ä½ æä¸é®è¿å¥ä¹åã ä¿®å¤äºæ¡èµ·åå¤å¿æ¶åºç°çå´©æºé®é¢ ä¿®å¤Mac/Windowsçå¨å±æ¨¡å¼ã ä¿®å¤äºæ é¢çé¢çæå¯¼ ä¿®æ­£äºæ é¢é¡µé¢çæç¤ºï¼iOSï¼ æ¹æ­£äºå¤ä¸ªz-orderçé®é¢ã ä¿®å¤äºä¸äºå¯¼è´å´©æºçbug æ·¹æ²¡ä¹ç©´ ä¸è¯¦ä¹æ£® å¹¸è¿çæ¯ï¼é¿åå¯ä»¥ä»å¢ä¸ååº¦èµ·è·³ï¼ å·æ³åå¶ä»åæ°´çå°æ¹æ¯å¾æç¨çã é¿åçæ´ç©´ éèé¿åæ¯ä¸ä¸ªå¸åçäºç»´å¹³é¢å¨ä½åé©æ¸¸æï¼æ å¿è§è²å°±æ¯è¿åªèªå¤§çéèé¿åãæ¸¸æå¶ä½å¢éåæ¶ä¹æ¯é¦è¯ºä¹æçä½èï¼ä¸ç´æ²æµ¸äºé«è´¨éçåç´ èºæ¯ãè¿ä¸ªæ¸¸æçä¸»è¦ä¸¾å¨å°±æ¯åå¤å¥è·åè¹¦è¹¦è·³è·³ï¼æçæ­¤éèå®æä»çåâé²âæ´»å¨ã é¿åï¼
ä»å°±å¨è¿éï¼ é¿åçå°å± é¿åçæ¿é´ é¿åçè§è²é²æï¼ä»ä¼è¹¦ãä¼æ¸¸ãä¼ç¨èå¤´æè«å­ï¼ä¸è¿ä»è¿æåè«å­çå¼ºå¤§åéãè¿ä¸ªæ¸¸æåæ ·æè´­ä¹°ï¼è§£å¯ï¼æ¢ç´¢æåââä¹æ¯ä¸ä¸ªç¹æçä¸çã é¿åçèå¤´åå¾æ´çµæ´» é¿åçä¸çæ¯ä¸ªå¾å±é©çå°æ¹ã éèç¾é£ è¿ä¸æ­¥å å¼ºå°ä¸å¢å£å¾åï¼æ·»å è¾¹ç¼ï¼ å æ¯é¡¿ççé²é±¼åºè¯¥å¾å¿«å°±å°ã å¼ï¼å¨é£éè°æ¾å°è°å¢ï¼ åï¼è°¢æã ççåï¼æçæ¯å¾æ±æ­ã å°çåæ­¥è½¨éè¾¾æ  éªå¼ï¼ æ­£ç®çå¢ã
ä¸è¿è¿å¾ç­ä¼å¿æè½å¥½ã è°¢å¤©è°¢å°æè¿½ä¸ä½ äºï¼
ä½ æå¥½æ³¨æç¹â¦â¦ é´æç©ºå° ç³ç ºåä¸ ç´æ¥å¾åèµ°ã å¤©åï¼ä»å¤©çä¸æ³å·¥ä½ã
æå°±æ³ç¡è§ã é»éå³¡è°· åºå»ææ¸¸åå£ï¼å»ççå²ä¸é£åã
  ç¥ä½ å¥½è¿ï¼ å¥½è¿ã æå¥å¿ åæ²¡ï¼ éèå°é æ¨å¿µ å¿ï¼ç­ç­ï¼é£ææä¹åï¼ï¼ åï¼å¥½å§ï¼æä»¬ä¹ççè¿ä¸ªå³å®ã åï¼ ä½ è¯´âå¥½å¥½çâï¼
æè¿å°±å»ï¼ æ¬æå¹³å° ä»æ¾ç¤ºè¿è¯æ®åï¼
æ¯å¦çªç¶è±åºå¾å¤é±ï¼ ç¥ä½ ç©å¾å¼å¿ï¼ ä»é®æç¥éä¸ç¥éè¿æ¯è°ã é¦åï¼ä»éæ©æä»¬çå¤å½¢ã
éå¸¸ä»å¨ç©çæ¾çµæã ä»å¨å¤§äººç©ä¸­å¾æåæ°ï¼ä½ä½ ç¥éä»æ¯è°ã å¨è¿æ¹é¢ä»èªç§°ä¸ºèºæ¯å®¶ï¼
ä¸ºäºèªå°å¿ï¼æä¹ä¸è¡¨è¾¾å¼è®®ã ä»ç»æé±ï¼ç¶å'å³å®'ææ¬ ä»å¤å°ã ä¹è®¸ä»ç¨äºé±å»ºäºè¿ä¸ªï¼ä½ä»æ¿å­ä¹æäºå®
å¨çæ¿é´ã ä»è¦æèµä¸ç¬é±å°è¿éã ä»è¦ç¨è¿ä¸ªä½åå£å é¢æéã ä»åªæ¯ä¸ªè ¢èã
è«åå¶å¦å°åç¯ççäº... ä»åä¹ä¸æ¯æçæåï¼
ä½æ¯ææ³ææ¸æ¥æ¯æä¹ä¸åäºã ä»æ¯æçç«ä¹ä¸ã
ä»æä»¬çä¸ä¸ªå·¥ç¨å¸åé¨åºæ¥ã ä»ä¸ªåå¬é¬¼ï¼ä½ ç¥éã åç½ï½ ä½ å¥½ï¼åä¸æ¬¡ï¼
ç°å¨å¦ä½ï¼ ä½ å¥½ï¼  æ¯ç«æä»¬é½å¨è¿éï¼ä¸æ¯åï¼ å¿å¥å¥ï¼ä½ å¨åä»ä¹ï¼ å¥½åï¼åå¼ä»¬ï¼ åï¼ä½ ä»¬è¿å¥½åï¼ å¿ï¼ å¿ï¼å¨è¿éï¼ å¿ï¼
ææ­£è¯çå¸®å¿ä¿å«å°éã å¿ï¼é¿åã å¿ï¼æµ·åï¼ä½ è¿å¥½ä¸ï¼ å¿ï¼æ©é¥­åå¤å¾åæ ·äºï¼ åï¼ä¼è®¡ï¼ æè°¢ï¼ä½ ä»¬çæ¯å¥½å®¶ä¼ï¼ å¿ï¼æå¥æ°é»ï¼ å¿ã
æ¬¢è¿åä¸´å°åºã ä½ å¥½ï¼æä¹æ ·ï¼ å¨ï¼å·§æ¯ã å¨ï¼ææ¯é¿åã åè§é¢äºï¼ä½ å¥½ã ä½ å¥½ï¼ä½ å¨å¹²ä»ä¹ï¼ éç§æ´ç©´ éç§æ·±å¤ å¯ï¼æä»æ¥æ²¡èèè¿è¿ä¸ªã æ©ï¼ä½ æçï¼æç¥éç¹ä¸è¥¿
ä¹è®¸å¯ä»¥å¸®ä¸å¿ã å... åªéå¼å§å¢ï¼ åâ¦â¦ ç«¥åæ æ¬ºï¼ä»·å»ç©ç¾ï¼â è¯´å®è¯ï¼å¬ä»ä»¬è¯´è¿ä¸ªæºå¯ç¬çã
ä¿å®çæäºäººå®å¨æ¯æ··èã ææä¹ç¥éè°æ¯åå¥½çï¼
è°ä¸æ¯ï¼ çæè¶£ã ä½ ä¸ºä»ä¹å¨åæ°´æï¼ ä½ åå¤ä¸ºæè«ä¸å®¶åºå¤å°é±å¢ï¼ å¯¹ä»ç±³æ ¼æå§çäºæè¿è¡å¾å¦ä½äºï¼ ä¸è¿ï¼å®ä»¬åæ ·ä¼¤å®³æäººï¼ æ...æ²¡...ç¨ä»ä¹å·¥å·ã
èµ°çæ¥çã æ... æ²¡çå°ä»ä¹è¿æ¥ã æ¯â¦åãåâ¦ä½ ä½å¨è¿éï¼ æ¯æä¹ï¼ ææå¥½è¿æ¯æè¿é¥åçå¨è¿éã å»å°éçè·¯ä¸æä¼è·¯è¿
å¥å¥çå®¶çï¼ ææ²¡æ³å³å®é£äºå¥½ã æä¸ç¥éã ææ¿è®¤ï¼æç¡®å®ä¸ç¥éä»-æä¹-å¶é æä»¬çï¼
ä½æ¯æç¥éæ¯ä»ã ä½æ¿å¦æ­¤ã
ç¥ä½ å¥½è¿ï¼ æä¸å³å¿ä½ å¨åå¥ä¸è¥¿ã
å»åç»å«äººå§ã æå¯æ²¡å¿è¦å¿åè¿ç§äºã ä¸è¿æä¸ç¥éæ§å¶ç³»ç»å¨åªéã
æç©¶ç«ä¸æ¯è¿éçäººã æä¹ä¸ç¥éâ¦å°åºè¿æ¬¡æ¯ä»ä¹åå ï¼ æä¸ç¥éï¼è¿æ ·æäº æä¸è®¤ä¸ºä»éè¦ä¸ä¸ªåå£ã
å¦æä»æ³è¦å é¢ï¼ç´æ¥å°±åå°äº... æçä½ è¿æ²¡è½æå°éèèæ¯å§ï¼ æå¯ä¸è½åä»ä¹ä¿è¯ï¼
ä¸è¿æºä¼å¯è½ä¼åºç°å§ï¼
è°ç¥éå¢ï¼ ä¸è®¤ä¸ºå¯è¡ã æè§å¾å®ä»¬ä¸ç¥éè¦æ¾ä»ä¹ä¸è¥¿ã
  æç­å¾ä¸èç¦äºï¼
æä»¥èªå·±ä¸ä¸ªäººåäºã çæ¥æä¸å®è¦é®ï¼
è°åä½ å»ºè¿ä¸ªçï¼ ææ³å°å¦ä¸è¾¹ï¼ç±³æ ¼æå§çä¸¾å¨ã æçåºå»çå¯ä¸æ¹æ³å°±æ¯ââ
è¿å»ï¼ æè®¨åè¿ä»½å·¥ä½ã
é£éé¢è®©äººæ¯éª¨æç¶ï¼ ææä¸ªæ³¨æï¼
å¦æä½ è¦å»ï¼æå¥½å¨ä¸åé¡ºå©çæ¶åèµ°ã æå®å¨ä¸ç¥éä½ å¨è¯´ä»ä¹ã ææç¹æ­£ç»äºè¦å¤çã ææå¾é¿æ¶é´æ²¡å»é£å¿äºï¼
æä»¥ä¸æ¸æ¥é£éç°å¨æäºä»ä¹æ ·å­ã æå¹¶æ²¡ç»ä½ æºä¼éæ­ï¼ä¸è¿ç®äºï¼è°¢è°¢ä½ èµ¶
èµ°ä»ä»¬ã æå¬è¯´ä½ å¸®äºå¤§å¿æé£äºå®¶ä¼åè·äºãå¹²çå¥½ï¼ æå¬è¯´ä½ è®©ä»ä»¬éå¸¸å¤´çã æè¡·å¿ä¸æ¿çµæ¯æ­¤äºï¼æè®©å«äººå¨æè´è´£
è¿ä¸ªã å¸æä»ä»¬èè¢è®°äºã æåæ³å°å¯ä»¥æ¯å¤©å¤§çå¤æï¼
è¿æ¯äººäººèº«ä¸é½æ²¡è¡¥ä¸æå¥½äºã å¬èµ·æ¥å¾å»ä¸æ¯ï¼
å¶å®ä»å¾å°çæ¶å... æç¥éä½ å¨èçä¸è¥¿ï¼ä½æ¯ç»æç¹çº¿ç´¢ã ææçï¼ æåæ¬¢ä½ ï¼å°éèï¼
ææ°é­ã æè¯´ï¼å³ä½¿æä»¬æ­¦è£äºæéï¼æ°éä¹æ¯ä¸
ä¸ã ææ¯è¯´ï¼ä»æçè§åº¦æ¥çï¼
é£ä¹æ¯ä¸å¯æè®®çã æçæææ¯ï¼è¦ä¸æä»¬è¯¥å¹²åï¼
å¨è¿å¿å»åçï¼ æçæææ¯ ... å¨çº¸ä¸ï¼
è¿å¬ä¸å»æ¯ä¸è¿çè®¡åï¼ æå¾å¾ä¸èµ°â¦â¦ä¸å®æä¸ªåæ³
å¯ä»¥å¯å¨åéæ¢¯çã æå¾å°éä¸
åäºäºã ææè¯å°è¿å¨æ¯è½¨è¿¹ã
ä»ä¸äºä¸ªå½é¢ï¼ä½ ç§ã æè¯´ï¼æ¯è°ï¼ï¼ æç¥éäºã æ¯åâ¦ æåºæ¥å°±æ¯ååï¼ä½ ç¥éä¹ï¼ é£ä¹æ¯å¿é¡»æäººå¹²çæ´»ã æè¦åï¼
éº»ç¦ - æè¿æ ·è¯´ï¼æå¹²å®äºï¼ ææ³æå°±ç»§ç»­ç¡å§ã è¿ææçå¾æ¯é²é¸çæ ·å­ã æè®°å¾ä»ä»¬è´´äºä¸ä¸ª
å³äºä½ çå®å¨å¬åã ææ³å®ä»¬åªæ¯éµå®âç¡®ä¿è¿éå®ç¨³âçå½ä»¤ã
  ä¸éï¼ ç¥éäºã ææ³è¦é®ä½ æéçäºä»¶ã ç¥ä½ å¥½è¿ï¼
å¸æé£ç©æå¿ä¿æ¤ä½ çå®å¨ï¼ æä¸ä»æä¸äºäººè¿ä½è¿ä¸ªï¼æ­£ç¡®éè¯¯é½ä¸
æç½ã ææ²¡æèç®ï¼æä»¥æåªè½å¸æ
ä½ ä¿æåå¹³ã ææå¥½å°å¿ç¹ã æ±æ­ï¼æç°å¨å¾å¿ï¼æ²¡æ³é¿èã æçº¦è®¡å¤§æ¦38ï¼ï¼
çº¦15çå¢éã ææ³è¦åäºäºæï¼æ¯å´æé... é£æå¾ä¼å¿ååæ¥ççã æä¼å»åç±³æ ¼æå§è°ï¼
è§£å³ææçéº»ç¦ãæç½ï¼ æä¼æ³¨æï¼è°¢è°¢ã ææ¯é¿åã ææä¸ç­ã æåä½ ä¸æ ·è®¤çï¼è¿ä»¶äºæä¸æä»¬åä½æ´å®¹
æå°è¾¾åºé¨ã æä¹æ³ä¸åºä»ä¹è®¡ååã æå¨æ´è¡£æï¼ æå®³æåç¨äºã åªè¦å¤§å®¶æ²¡äºå°±å¥½ã æå¨åèç³ã
æ³æ¿ä¸ç¹ä¹ï¼ æä¸ç¥éå®å·ä½æä»ä¹ç¨ï¼
ä½æ¯å¬è¯´å®å¥½åæèªå·±çæè¯ã ææ²¡æç½... æä¸å¤ªæç½â¦ æºå°çã æä¹å¨æ¿æ²»ä¸è¿æ¶äºï¼ä½ éè¦é®é®å¶ä»äººèµ°
ä»ä¹è·¯ã æå¨è¯»ä¹¦ã ææç®æ½å¥é£éï¼å ä¸ºå¬è¯´
å ä¹ä¸å¯è½ç¨éå¸¸çæ¹å¼è·ä»è§é¢ ã æè½å±è½ä¼¸ã ææ­£å¨åçå¢ã æå¨åªåã
é£äºå®¶ä¼é¿çåå®³çå´å·´ï¼ä½ ä¹ç¥éã ææ­£å¨æ³è¿ä¸ªé¨åï¼ææè§äºã æä»æ²¡å¬è¯´è¿è¿äºã éå¸¸æè°¢"å¤©ç¼"ã
å¥½ä¸è¥¿ã æè§è¿ä¸æ¬¡ï¼å®å°±å¨æ¬å´ä¸
å·¦è¾¹çå±å­éã å¦ææè¯´æ¯ï¼ä½ ä¼å«ä»ä»¬åï¼ æè¦æ¯ç¥éäºï¼é£è¿å°±ä¸æ¯
ä»ä¹ç§å¯äºï¼æ¯å§ï¼ åå¦æä¹è¿ä¹æé­åï¼å°±è½å½ä¸ééçæ¨¡è
äº... åå¦ææ²¡è®°éï¼
ä¸æ¡è·¯éå¾ä»çåå ¡ ... ä½ é¢ã å¦ææäººé®ï¼ææ²¡è§è¿ä½ ï¼å¥½å§ï¼ ä¸è¿è¦æ¯æ²¡æå»ä¸­ï¼å®ä»¬ä¼ææ¶å¤´æï¼ç¶å
ç¬èµ·æ¥ã è¥æ¯æ¾èå¤´ï¼ä¸é¡¾å¶ä»äººï¼æä¸ªåæ³è®©ä»ä¸
è½éã å¦æä½ å·²ç»ç¥éå¦ä½æä½ï¼
ç´æ¥èµ°å°ä¸ä¸å³å°±è¡äºã ä½ é®æï¼æè§å¾å»å¬ç±³æ ¼æå§çæäºã ç¢°å°åä¼ä¾é å¢ç¼æ¢ä¸æ»ã åå¦ä½ æ²¡æå©è·ï¼ä»ç¶å¯ä»¥æä½æåå¢å£ç
æ¹åé®ï¼ç¶åèµ·è·³ã å¦æä½ ä¸ä»ææé®ï¼è¿éæ¯åªï¼ å¦æå¨é£éé¥®æ°´çè¯ï¼
ä½ ççå½å¼ä¾¿ä¼å®å¨åå¤ï¼ å¦æä½ ç¹å¤´ï¼
æä»¬ä¸ææå°±å¼å·¥ã åå¦ä½ è¿æ²¡æ³¨æå°ï¼èä¸éççº¢è²æå®å¯ä»¥å·åºã
  åå¦ä½ æä½ä¸åæ»å»é®ï¼å·å°çå¨åä¼å¤§
åã åå¦æä¸ä¸å·åºï¼ä¼æè¾é«çè§åº¦ã å å¥ä½ æ³è¦ï¼æ½å¥æ¯æå¥½çã
æä¸è§å¾æ´å£å®å«è½éº»ç¦ä½ ã å¦æä½ æ­£æç®è¦è¿å»ï¼
æå¥½è¿æ¯åèèä¸ä¸ã å¦æä½ çè¿ä¸æ®µå¯¹è¯ï¼çç¬¬äºéå¯è½æ 
èã æ¹è¿æ£®æèæ¯ï¼ä½¿å¶è½æ éåä¸å»¶ä¼¸ã å®éä¸æ¯æå¸¦çè¿ç¹å­å»æ¾ä»çã
è¿æ¬è¯¥ç®æ¯æåäºä¸çã æè¡ä¸­ï¼å¾å¤æ³è¦æ¬ºåä½ çå®¶ä¼ã å¨è¿æé´ï¼å»æè®¿ä¸ä¸è¿è¾¹çå±æ°
å¯¹ä½ ä¹è®¸æå¸®å©ã ä¸å¯æè®®ã åå§åçé¢ åå§åèªå®ä¹å¯¹è±¡å½æ° åå§åæè´¨è´´å¾ åå§åå¾å ç¹é å¯ä»¥æ³è±¡ã
è¿æ ·ä¹è¯´çéã æ²¡äºå¿å§ï¼ å¦æææºä¼ï¼æè½...åä½ è°è°åï¼ ä½ è½åæ¥å¸®å¸®æä»¬ä¹ï¼
  æ²¡ä»ä¹ç¹æ®çç¨å¤ââä½æ¯è¿å¯æ¯é»éã
ä¸ä¼æåå¤ï¼ ç¡®å®æ¯ï¼å­©å­ï¼ æ¯ãæä»¬å°±çæ´»å¨ä¸ä¸ª
èè°¬çä¸çéï¼åï¼ è¿æ ·åç§æºæ¢°é½å¯ä»¥ä¸èµ·è¿è½¬ã ä¸é¢åçï¼
âå·§æ¯çä¸è½ååºï¼
å°±å¨åæ¹ï¼ ä¸é¢åçï¼âæ³¨æï¼
ç¨å®åè¯·æ¸ç
å¿ç«å¨æä»¶ç®±ä¸â ä¸é¢åçï¼âæ³¨æï¼æ¬åºåé«åº¦æªå®æãâ ä¸é¢åçï¼âä¸­å¤®å¤§é¨æ§å¶ç³»ç»â
æ­£æ¯æä»¬æ¾çï¼ ä¸é¢åçï¼âéååå¡â ä¸é¢åçï¼âéåæ´ç©´å¥å£â ä¸é¢åçï¼âéåå°çªåå¨èé´â ä¸é¢åçï¼âéåè¥¿å¡ãçåº§å®¤â ä¸é¢åçï¼âä»å¤©çç¹é¤ï¼å¯ææ²æåçæ²¹æå·â ä¸é¢åçï¼âæ³¨æï¼æ¬å®¤ä¸ºååéè¡â çèµ·æ¥æä¼¼ä¹
è¢«å°å¨è¿å±å­éäºã çèµ·æ¥å°ç«ä»¬åå¤ç¦»å¼äºã ç°å¨æ²¡é®é¢äºï¼
çè°¢è°¢ä½ æè®­é£å¸®å®¶ä¼ï¼ ç»æä»èº²å¨æä¸ªç§å¯ééã è¿é¡¹å·¥ç¨è¦è±å å¨ï¼
è¿éè¦å°å¤äººåï¼
ä½æ¯åºè¯¥å¼å¾ã é£æ ·æ´å¥½ï¼å°å¤æ£äºé±ä¹è½è®©æ°ä½æ·ä¹æèªå·±çææï¼
åä¸åæ¯æ½èã è¿å¯æ¯ä¸ªéè¦çé®é¢ã
åºè¯¥è®©è°è°æ¥è°æ¥ã æ¯ç±è¿å°çµèæ§å¶çã å¾å®¹ææè·¤ã
æä»¬è¿æ²¡æåæ­¥è¡æ¸¸å®¢çåå¤ã çä»ä»¬è¿æ ·åå°æ¥åºï¼
ä¹æºæè¶£ã è¿æ¯æå·¥è¦ç²¾ç¡®çå¤ï¼æä»¥å¾éè¦ã çæ³ï¼ä»å¯è½æå¶ä»çèå¨éé¢ã è¿æ¯æèªå·±çå®å¨å±ã è¶è¿ä¸é¾ï¼ææä¹ä¸é¾ââè¦æå°±ç¨è
å¤´ã æå¯è½ã å¯è½è¿æ ·æå¥½ï¼å¶å®ã
æå¯ä¸æ³è±¡äººä»¬ç¨ææ¶è§£å³äºç«¯çéº»ç¦äºã é£æ¯æè½åçã
å¸¦çä½ é£ï¼ææä¸å¯è½ã é¹¿è§å åï¼æ*åèª*è¿å±å­ä»å¤é¢ç
å®å¨æ²¡æè¿ä¹å¤§å¤§å¤§å¤§å¤§ååï¼ è·³è· é®æææäººæ¯ä¸ªå¥æªçæ¹æ³ï¼ä¸è¿è¿ä¹å¤äºº
æå¯ä»¥ç¨ä¸åä¹ä¸ç¨å¥ã å°±æ¯å»éä¸çæ°é»ã å°å¿ç¹å¿ï¼ é®çéæ°æ å°ç¼ç¨ è¿ç»­åæ10ä¸ªç¸åç±»åçæäººã å¨åç§éä¹ååæäºä¸ªæäººã ææåå ç«ç«å®¿è å³äºè¿å±å­ï¼
ä½ ç¥éä»ä¹æç¨çä¸è¥¿ä¸ï¼ ç¥éå§ï¼ä½ åºè¯¥èµ°è¿å»å¸¦åºæ¥ã è¯»åæ¸¸æ ä¸»ç¨åºå åå°å±é©é¢åæ¯æè±å®ä»¬çåæ³ä¹ä¸ã å©ä¸æ¥çé½ä¸¢å°ç®±äºã æççâ¦åâ¦
å¯¹äºï¼29åºçåéã å³å¡è®¾è®¡åå¯¹è±¡èæ¬ç¼å å³å¡è®¾è®¡äººå â¢ æ£®æãæ´ç©´åå°ä¸å³å¡ å³å¡è®¾è®¡è â¢ æ²¿æµ·ãæéãæ£®æ æåæä¹è¯´äºï¼æèªå·±ä¹ä¸ç¥éã æ­£å¨è¯»åè§è²ï¼é¿å æ­£å¨è¯»åå³å¡ è¯»åæè´¨è´´å¾. è¯»åæè´¨è´´å¾.. è¯»åæè´¨è´´å¾... å¤§å å¾å¤ç»å¾®çå¾åå¢å¼ºã è®©é»éèèå°äº ä½¿å¾åå¤å¿æ¢å¤ä¸æ ¼å¿ï¼ä»¥åæ¯åæ ¼ è¿æ ·ç¥è¿å¯¹è¯åºæ¯ã ä½¿å¾ç©å®¶ç¶æä¸å¨æ é¢çé¢æä¸çå°å¾æ¾ç¤º æ¯ä¸æ­¥é½è½çå°ç¯çåå¥è¿¹ã Maemoç§»æ¤ ç»´ä¿®ç®¡é ä½ æå¥½æ¾èªæç¹å¿ã æéçã å¾å¤å°bugä¿®å¤ å³å¡ä¸­ç°å¨æ£è½äºå¯ä»¥ååºçéå·ã ä¸è¿éé£è®¾å¤åæ¯è¶ç»åçã è¯´ä¸å®æå¯ä»¥è¯´å¨ä»ï¼ åï¼ä¹è®¸å§ã ä¸é¨å³äºæçï¼
è¿äºæä¸ç¹å°´å°¬â¦ æï¼æå¯ä¸æ³è·ä½ ææ¶ã
åè¯´ä½ çèµ·æ¥äººä¸éã åæ¶â¦ æè®¸ææ¡¥ï¼å¼æ ¼æ¯æ­£è¦ç«èµ·ä¸ä¸ªã ç±³æ ¼æå§æå°éçè¿ä¸å
å°éå¾ä¸¥ä¸¥å®å®ï¼ä»è¿æäº
å¤§å¤æ°å±æ°åäººè´¨ã ç±³æ ¼æå§çåå ¡ ç±³æ ¼æå§ççåº§ä¹é´ ç±³æ ¼æå§ççåº§å®¤å¥å£ ç±³æ ¼æå§çå±ä¸ãå½é£ä¸ªå¤´ç®æ ·çå®¶ä¼åºæ¥
ä¹åï¼é½è¹¦ä¸ä»çé£è¹ç¦»å¼äºã ç±³æ ¼æå§çäººä»æ¨å¨æéç°èº«äºï¼
å®ä»¬æææäººé½ç¾æ¼äºï¼ ç±³æ ¼æå§è¯å¾å°éæéï¼
é£æ ·æ²¡äººå¯ä»¥åºå¥ã å©çå³å¡è®¾è®¡å¸ å©çå¯¹è±¡èæ¬å¸/å³å¡è®¾è®¡å¸ ç»å¾®å³å¡åå¨ã æåå£å é³ä¹é³éï¼ é³ä¹å¶ä½è æ°æ¸¸æ ä¸äºï¼æå¾å»éå­ä¸ã æ²¡æï¼è¿ä¸ªéå­æ²¡æé£ä¸ª èªç¶ï¼ä»ä¹é½æ²¡åçã å¹³å¿è¯´ï¼å¦æä¸æ¯ä»ï¼æä»¬ä¸ä¼æè¿æ¬¡è°
è¯ã èªç¶ä¹å­ å¥å¥çå°å± æ²¡æã ä¸è®ºå¦ä½ï¼è°¢è°¢ä½ çå¸®å©ï¼ ä¸è¿ï¼å æ¯é¡¿æ¯ä¸ªä¸éçèæ¿ã ä¸ç®¡æä¹è¯´ï¼ææå¥½è¿æ¯èµ°åã
å¦æé¡ºå©çè¯ï¼æ¡¥å¾å¿«å°±è½ä¿®å¥½äºã
è¿éåº¦çè®©äººå°è±¡æ·±å»ï¼
ä»ä¹æ¨¡ååå»ºé ä¹ç±»çã ä¸è¿ï¼ä½ è¯ä¸è¯åºè¯¥å°±
å¯ä»¥ç¢ç£¨åºæ¥äºã æ°ç¿»è¯: æå¤§å©è¯­ æ°çå¯¹è¯æ¡å­ä½ã ç¶åä¸ç¥æä¹çï¼å°±æäººåæå¼æªï¼
æè¦ç¥éåå ï¼ æ³å¾ç¾â¦â¦ ä¸æ¯ å®å¨åæãå¿ï¼ä½ æ¢æ¢åã
è°¢è°¢æä¾è¿äºä¿¡æ¯ã è¿éæ²¡æä¿å®äººåï¼
æä»¥æä»¬ä¸ä¼æä½ ã æ²¡é®é¢ï¼ æ²¡é®é¢ã æ²¡é¨ï¼é¤éææäºï¼ ä¸ï¼ä½ é¾éæ¯â¦â¦ æ²¡æï¼ææ²¡å¬è¿ã ä¸ä¼ï¼æå¯ä¸æ³æ¾éº»ç¦ã ä½ å«è¯¯ä¼ã ä¸ï¼ä¸â¦â¦
åªæ¯æå¸ææä»¬æä¸å¤§ç¾¤äººä¸èµ·é¯è¿å»ã åä¹ä¸ï¼æçå¯å¨å¸é¿ï¼ä½å¨äºå®ä¸ï¼ææ¯è·æçã æ²¡ç¨ï¼éä½äºã ä¸è¡ãä½ è¦ååç­æçé®é¢ï¼ ä¸è¡ï¼
ä½ åªè½é èªå·±ãå¥½è¿ã æä¹ä¸æââåªæ¯å«äººè¿æ ·è¯´çã ååº­é¢ æ²¡äºå¸½å­å¯ä»¥ä¸è¡ï¼ ä¸æ¯å¾æ¸æ¥ã å¹¶ä¸æ¯è¯´æä¸å®å¾èµ·æ¥ï¼ ä¸è¿è°ä¹æ²¡é®æçæè§ã ä¸å°æ¶åã
æ±æ­ã ä¸-æ-å´-è¶£ï¼
æè¯´å®äºã æ³¨æï¼è¿äºä¸æ­£å¸¸çä¸è¥¿ï¼
åå¦ç¨å°½äºï¼ç¦»å¼åºæ¯åè¿åå¯è½å°±åæäºã æçåé¢éæ²¡å¥ä¸è¥¿å¯çã æ­£å¨å¯å¨äººå·¥éåã ç°å¨ä¸é¢æä¸æé¨ã
è¥æ¯ç©¿è¿å®ï¼å°±å°äºç³ç°å²©æ´çªã
æ®è¯´é£å¿æä¸æ¡è·¯éå¾ç±³æ ¼æå§æ®ç¹çåºå±ã
  ç°å¨ååï¼ å¯¹è±¡èæ¬å¸ å¯¹è±¡èæ¬äººå â¢ ç¹æ§åæå°± å½ç¶ä¸æ¯ï¼æºå¨äººä¸åä¸è¥¿ã
æä»¬æ¯çç©ã å½ç¶æ²¡åå°ã
ä½ çèç¼ã å½ç¶ã
æä»ä¸è®©äººå¤±æï¼ è¯¥æ­»ï¼èå¤§ï¼ä»å¨è¿å¿ï¼ ææä½ ä¸è¦åè·æèææäºï¼ å¦è¯¥æ­»ï¼æç¡è¿å¤´äºï¼ å¦ï¼ä½ å¥½ï¼ä»æ²¡è§è¿ä½ å¢ã å¦ï¼ççï¼åªéï¼ å¦ï¼è°¢å¤©è°¢å°ï¼ä½ è½è®²è¯ï¼ å¦ï¼è¿æï¼ææ²¡æå°ï¼
ä»è¯´æ¯æç§âæèµâã å¦ï¼ææ³ä½ å¤§æ¦ä¸ç¥éè¿ä¸ªâ¦
æä»¬é½ä¹ æ¯äºã ææ­£åå¤åé¥­å¢ã
ä¸èµ·æ¥åï¼ å¦ï¼å½ç¶æ¯ç±³æ ¼æå§ã
å¦åä»ä¸ºä»ä¹ä¼è¿ä¹éè¦ï¼ å¦ï¼ä½ å¥½ï¼ä»æ²¡è§è¿ä½ å¢ã å¦ï¼ä½ å¥½ã
ææ­£å¨åæ°´æã å¦ï¼ä½ å¥½ï¼
æ²¡çå°ä½ è¿æ¥ã å¦ï¼ä½ å¥½ï¼ å¦ï¼ä½ å¥½ãæå¨å å·¥éå±é¨ä»¶ã å¦ï¼å¨ï¼è¿å¿æä¹äºï¼ å¦ï¼åªä¸è¿æ¯å¨æ¶æ¾åµé¹åççæå­ã å¦ï¼æ²¡é®ä½å§ã å¦ï¼ä½ ä¸è½è¿ä¹å¹²ï¼ å¦ï¼é£ä»æ¯ä½ çç¶äº²ï¼
æå¾å¤çå°å­©èå·²ã å¦ï¼å¥½å§ï¼å¨ä½ åºåä¹å
ä¹è®¸å¯ä»¥å»ççæçå°çªã å¦ï¼çæ¯å¯æã å¦ï¼ æä¸ç¥éè¯¥æä¹è¯´äºã
é£å¬ä¸å»ä¸å¯æè®®ã å¥½äºï¼æåå¤äºï¼è¯¥åå®¶äºã å¥½å§ï¼ä½ ç¥éâé«å©è´·âåï¼ å¥½å§ï¼é£åé¥­åå¤å¾åæ ·äºï¼ å¥½å§ï¼åï¼è°¢è°¢ã å¥½å§ã
æå¾ç»ä½ ä»ä¹ï¼ å¥½å§ãæ¨è½ä¸è½æé¥å
åç»æå¢ï¼ å¥½å§...åªè½è¿æ ·äºï¼å°½ç®¡æäºæ·±å¥¥ã å¼å§ï¼ä¸è¯¾æãæ¯ä¸ªé®å¥½é½æç­æ¡ã åå°çä»»ä½ä¸è¥¿é½æ¯æ­¦å¨ã æ¯ç±³æ ¼æå§çä¸ä¸ªå°å¼ï¼æ¯«æ çé®ã ç®æå§ã OpenFeint å¾ææ¾ï¼ææ¯åªé¼¹é¼ ã ä¸äºå°çbugä¿®æ­£ å¦ã æä»¬çç©å®¶ éè¿ä¹åï¼ä½ ä¼å°è¾¾ä¸æéã å¸æçæ¿é´ ä¹è®¸æ´è¦ç´§çæ¯ï¼æç®æ¯ä¸ª
èä¸çå¤©æå­¦å®¶å¼çç©å­¦å®¶ã å¸ã æ¾æå¬å­ çç«ç¿äº å¹³å°åé© å¼å§ï¼ ç©ç»æµçæ¸¸æï¼æ¯åï¼ åè¯´ï¼å®ä»¬éåº¦å¿«ã
é£åå¥çã é¶è±é± æå¯è½ï¼ä½æ¯å¯è½æ§å¾å°ã
ä½ åºè¯¥è½å¨è¿éä¼æ¯ä¸éµã æ${DOWN_KEY}è¶´ä¸ã æ${JUMP_KEY}è·³è·ã æ${LEFT_KEY}æè${RIGHT_KEY}å·¦å³èµ°å¨ã æ${TONGUE_KEY}è¿è¡è§è²ç¹æçæ»å»ã æ${UP_KEY}è¯´è¯åä½¿ç¨ç©åï¼ä¾å¦é¨ã å¨ç©ºä¸­ä¾éå¢å£æ¯æä¸è·³è·å¯ä»¥åæ¬¡è·³èµ·ã ç¼ç¨å â¢ å¼æå¼ååæ¹å ç¨åºå â¢ é³é¢åiOSä»£ç  ç¨åºåï¼é¦å¸­åå®¹äºè®ºèåé¦å¸­ç©ä»¶èæ¬
ç¼åè æä¸åªæ°´ç²è«ä¸¢åæ°´éã è¿æä¸¤ä¸${LEFT_KEY}æè${RIGHT_KEY}å¯ä»¥å¥è·ã å¥è· æ¨ªè¡é¸éï¼ ççï¼ä»ä»æ²¡æèµ·è¿ä¸ªï¼ ççï¼ä½ çè¹åå¨åªéï¼ éå¶å ¡ååºæ¯å¢å£å¾è±¡ éå¶å°ä¸åºæ¯å¢å£å¾è±¡ éå¶æ£®æèæ¯çå¾è±¡ éå¶æ£®ææ æ¨çå¾è±¡ éç»ä¸çå°å¾ã éæäºæä¼¤åæªç©ç§ç±»ç³»ç»ï¼ä»¥æ¶é¤ä¸äºå°è§çéè¯¯ï¼å¢å ä¸è´æ§ã å ä¸ºè¿ä¸ªç¼æãæéè¦åä»ä»¬è¯´è¯´åºæ¬é
çã å¾å¥½ã
æè¿ä»¥ä¸ºä½ æ¥è¢­å»æã èåä½æã
å®¿èä¸æ ·ççå°æ¹ã è¿åæ¸¸æ åå°æ é¢ç»é¢ äº¤æ¢A/Bé® è¯è®ºï¼ éå¶âé»æâ éå¶æäºå¾åï¼ä¿®æ­£éè¯¯ éåäºå¤æ°å¯¹è¯ã éº»ç¦äºï¼ä¸æ¯åï¼ ææ»è¦å¡ å²©ç³æ æ ¹ å¥½ï¼ éæ©é¾åº¦ï¼ é¸£è°¢ï¼ é£ä¸ªâ¦â¦
èåè¾åäºï¼ èæ¬è¯­è¨è®¾è®¡å¸ éå±å°å± æå¾ä½ çè¡¨ç°ã çï¼ç±³æ ¼æå§å°±æ¯ä¸ä¸ªã å¥½å§ï¼ä¸äºå¹´åï¼
ç±³æ ¼æå§å¸¦çä¸ä¸ªææ¡å°æè¿éæ¥ã ä½ çï¼ä¸¥æ ¼çè¯´ï¼æä»¬ä¸æ¯ç«ã
æä»¬åªæ¯æç«çå¤å½¢å¶é åºæ¥çã é£ä¹ä»å°±å¯ä»¥
éææ¿èµ°æ³è¦çä¸è¥¿ï¼åä¸ºä»£ä»·ã éæ©é¾åº¦ï¼ éæ©å­æ¡£æ§½ï¼ éæ©ä¸ä¸ªå³å¡ï¼ éæ©è¦è¯»å¥çå­æ¡£æ§½ éæ©è¦ä½¿ç¨çå­æ¡£æ§½ å±ç¤ºä½ çæå·§å§ï¼ æ¾èçæ¹è¯äºæ§å¶ç³»ç»çåè½åå¤è¡¨(iPhone)ã ç®åå¾å¾ï¼é£è¿å»å°±æ¯äºï¼ é£ä¹ ... ä¸åé½å®ç»äºï¼ æä¸ç¥éé£ä¸ªå®¶ä¼å¯¹ä½ è¯´äºä»ä¹ï¼ä¸è¿è¯å®
æ¯åºè¯ã å°±æ¯è¯´æç»å°å¨è¿éäºï¼åï¼ é£ä¹ä¸åé½å¥½
æçæ²¡é®é¢ã å ä¸ºæ²¡æå¶ä»ä¸ªä½æ¿æï¼æå³å®èªå·±åå¾ã çæ ·å­è¿éæ¯é¥®é£åºï¼ å°±è®©æç´è¯´äºå§â¦â¦ æ¥å¸¸ååæ¥äºï¼çå¥½ã è¿äºä¹å¤çäºé½æ¯è°å¢ã æä»¥ä»è¯´äºä¸éæ··å¸çè®ºæ¥è¯æ
ç±³æ ¼æå§åå¤å é¢æéã é£ä¹â¦ä½ ä»¬æ¯æºå¨äººä¸ç±»çï¼ é¡ºä¾¿é®ä¸å¥ï¼ä½ æ¯åä»ä¹å·¥ä½çï¼ æå¨åªå¿ï¼ ä½ è¯´ä½ æ¯å¶é çâ¦â¦
é£ä¹æ¯è°å¶é äºä½ ï¼ ä½ å°±æ¯å°å¤å¶é éº»ç¦çå®¶ä¼äºï¼åµï¼ ç»äºï¼ææå¼äºè¿æé¨ï¼ é¢ï¼æè½é®ä¸ï¼
ç°å¨è¯¥å¹²ä»ä¹ï¼ åï¼ç°å¨æä¹æ ·ï¼ é£ä¹ä½ æä¹ä»ç¬¼å­éåºæ¥çï¼ ä¸ï¼å®ä»¬ä¸æ¯ä¸æã é£ç°å¨ååï¼ åï¼åé¥­åªéå»äºï¼ è¯´ä»ä» ç±³æ ¼æå§åäºé±ï¼æ´ä¸ªäºä»¶
å°±æ¯ç±³æ ¼æå§æ¥ä»ä»èº«ä¸æé±ä¸æ¥ã
  æäºé«åº¦æ æ³ç´æ¥æ¥è§¦ã æäºå±é©å°±æ¯å¦æ­¤ã
ä½ åªè½ç»è¿å®ä»¬ã æäºè¯´ç½äºæ¯è­¦ä¸æäºâ¦ æ»æä¸å¤©è¿äºææ¨éçå®¶ä¼ä¼ç¥é
è¿ç§å¼ºä¹°å¼ºåçç­ç¥åªä¼è®¨äººåã è¿å£çæç¹è¯¡å¼ã åæ¯ï¼ä½ ä¼è·è¿ä¸ä¸ªçèµ·æ¥å±é©çåºåã æ±æ­ï¼æå¾å»éä¸ï¼
æä¸æå´è¶£ï¼ å£°æè®¾è®¡å¸ é³æé³éï¼ å¬èµ·æ¥åä»ç»ä½³çèä½ï¼ ä»å°è¿ä¸ªï¼ææ³è¦æè°¢ä½ ä»¬çå¸®å©ã è¯´å°âæ¥å¸¸âï¼æ¯æçéè§å¢ï¼
è¿æ¯è¿å±å­ççåå°äºï¼ æå°è¿ä¸ªï¼ææºå¥½å¥ï¼
æä»¥ä¸ºç«æ¯é£èçï¼ä¸æ¯åï¼ äº¤æå¿«æ å¿«è·éå³ é£æºä¸è¹¦è·³ æåæ´ç©´ æ ä½åå¼å± åèååæäººé½æ¯ææ»å»é®æ¥è¿è¡çã ç³ç®¡æ¡¥ ç³ç®¡æ´¼å° å¨èé´ å¥æªå¢... æ¬¡ä¼è®¾è®¡ è¿éçæ¯è²å¤ç¾å¦åã çªç¶é´ä»ä»¬çä¸¾å¨ä¸å¯çè§£äºã æ²¡é®é¢ã ä¸å®... éä¾¿ççå§ã è¯´ä¸äºãçæä»¬è½ä¸è½è¾¾æåå¥½åè®®ã è°è°ï¼åç»å£«ä¸æ ·ï¼ ææ¯æå¯¼ åè¯æç»è¿ã è°¢è°¢ä½ åç¦æ¯ï¼è¿ä¸ªå¾ä¸éã è°¢è°¢ââææç¹å¿ï¼
ä½æ¯æè°¢ä½ çéè¯·ã é£ ... ç«éé¿ï¼ä¸è®ºæ¯è°ï¼æ³è¦ææåæè
å æä»¥ææ¬ ä»çé±ï¼ææ°¸è¿è¿ä¸æ¸ã è¿ç¡®å®â¦â¦
æä¹±äºæä»å¤©çè®¡åã è¿å®¶ä¼çä¸å»å¾æ¾ç¶
ä¸æä¹æ¬¢è¿æï¼ é£åä¹å¾æå¯è½ã è¿å°±æ¯æä½å¾è¿ä¹é«çåå å¦ã
å¨è¿ä¹é«çå°æ¹ä½ è½çå°å¾å¤ä¸è¥¿ã æéçã è¿ä¹è®¸å°±æ¯ä»ä¼å¾æ¯ææ¡¥ç
åå å§ã é£èè±è¸ççåå¤ç£¨ååºå¾ã å°±åæ´ä¸ªç»è¿æ ¹æ¬æ²¡æä¹ä¸æ ·ã è¿ä¸ªä½ç³»å¬èµ·æ¥ä¸éã å¬ä¸å»ä¸éåã ä½ çæ¯å¥½æ ·çï¼ å®è¯è¯´è¿åæ¯ä¸ä¸ªæææäºº
é½å´èµ·æ¥çå¥½åæ³å¢ã çæ¯ä¸ªå¥½æ¶æ¯ï¼ æç¨å°æ­¤ç»æã æé½ä¸ç¥å°ï¼
ä½ å¨åªéçå°çï¼ é£æ¯å¥½æ¶æ¯ã å¥½æäºã ä¸éï¼æ¯è®²ä»ä¹çä¹¦ï¼ çèç¼ã å¤ªæ£äºã
å... ä¸éï¼ å¯¹äºãæ¯æ²¡è¿æ°æ¶èå°ç½è§çéè²èèã
  é£æ¯æ éçç¬¼å­ã æææ... ä½ å¾æé£åº¦ã ç«å·¥åçç«æ­¥ å±é©ä¹å± èåè¾ä¹å± åéæ¢¯å°å± å§ç» å¥å£ ç«ç«å·¥å é¿éè¿è¾ åå¡ éå¾å°éçæ´ç©´é
é©»æçå¨åµã èåè¾çå±å­å¨ä¸è¾¹ï¼
è¿éå­çé¨å¾ä¸å»ã è½éæ°´æ¶å¯å¨è½éå æ¤ã
è½éå æ¤è®©ä½ è½å¤å°åº
è½éå¼¹ã å½ä½ è¹²ä¸æ¶ï¼è½éæ¤ç¾
ä¼ä¿æ¤ä½ ä¸åä¼¤å®³ã è¿å¹³é¢å¾çæ¯éª¨éª¼ç²¾å¥åã æé«çºªå½æ¯ç¬¬ ${registry.arcade_coin_race_high_score} å³ã  æ¸¸æå·²å­æ¡£ã æ»ç¿ç¿¼ä½¿å¾ä½ å¯ä»¥å¨ç©ºä¸­æ»ç¿ã çº¢å¿ä¼èµäºä½ ä¸ç¹é¢å¤ç
çå½å¼ã æé«è®°å½æ¯${registry.arcade_blocks_high_score}ã æé«çºªå½æ¯${registry.arcade_down_the_rabbit_hall_high_score} åã æé«è®°å½æ¯${registry.arcade_maintenance_shaft_high_score}åã é«åæ¯ ${registry.arcade_nutty_factory_high_score} ï¼ æé«è®°å½æ¯${registry.arcade_the_long_haul_high_score}ã æé«è®°å½æ¯${registry.arcade_upper_deck_high_score}åã æé«è®°å½æ¯${registry.arcade_wild_new_wonder_high_score}åã è¿äºæ´é½æ¯æ åï¼ä¸è´çå°ºå¯¸ï¼éåºæ åçèºæ ã ååºæ°´æ¶å¯å¨ååºå æ¤ã
ååºå æ¤ä¼æé«ä½ ç
ååºè½åï¼ä½¿å¾ä½ å¨å´ç
ä¸åä¼¼ä¹é½åæ¢ã å¥ä¾µèï¼æä¹æ¥å°è¿éçï¼ æ ææ°´æ¶å¯å¨æ æå æ¤ã
æ æå æ¤ä¼è®©ä½ 
æ æä¸å°ä¼å¿ã è¿æ´»åä¸ªååéï¼æ²¡æåå³è¯­æï¼ä½ä»å¹¶ä¸ä»¥æ­¤ä¸ºåå£ã å¨åé´è®©ä½ è·³å¾æ´é«ã ä»·æ ¼æ¯ ${final_price} éå¸ã è¶æ©è¶å¥½ã å éé´è®©ä½ è·å¾æ´å¿«ã å¢å å·åçå°ç¨ã å¢å èå¤´çé¿åº¦ã æçä½ è¢«ç±³æ ¼æå§çå°å³èµ·æ¥äºï¼
  åä¸æ¬¡ï¼å·®ç¹åéå¼ ä¸è¿ï¼è¿ä¸ä¸ä¹åæ»¡äºæå¥çäºã åé¢ä¸è¿å¤æäºèèã
è¯·éææ¿å®ä»¬ç»ç»æå§ã å¨å°éå¤é¢æä¸ªååºï¼å¾æ¹ä¾¿çã è¿æ¯æ²¡åæ³åæ­¢çï¼
ä½ ä¸å¯è½æ§æ¯èä¸ã ä¸å®æä¸ªåæ³è¿å»çã å¥å¥çå®¶ï¼å°±å¨é£è¾¹ï¼ ä¸å®æä»ä¹ä¸è¥¿å¯ä»¥æä¸ºæä»¬çä¼å¿ï¼
ä½ä¸æ¯âæ°éä¼å¿âã å«è·æèè±æã è¿å±å­éä¸æ¯åºè¯¥æä¸ª
ç§å¯æ¿é´çä¹ã è¿é½æ¯å¼¹è¯çæ¥æºï¼å ä¹ä¸ç´å­å¨ã å°å¤é½æ¯ç¯ççç¸å¼¹å®¢ï¼ è¿äºæ¥å­ï¼æä»¬è¿æ¯æ¹éçäº§çã
æ¥¼ä¸æä¸ä¸ªå·¥å
çäº§æ¯è¾æåçè®¾è®¡ã è¿ç§æ»å»å°ç¨é¿ï¼ç ´ååè¯å¥½ï¼
ä½æ¯ä½¿ç¨èµ·æ¥æ¯è¾æ¶èã ä»ä»¬ä¹è®¸çå°æå¨åä½ è¯´è¯äºï¼
æä¸ç¡®å®ã ä»ä»¬è¢«æå¨å±å­éé¢ï¼
è¿ééçå®ãå¦æä½ è½è§£æä»ä»¬ï¼
æç¸ä¿¡ä»ä»¬ä¼å¸®å©ä½ çã ä¸å¹¸çæ¯ä»ä»¬å¤§æ¦æ
è­¦æ¥ç³»ç»ä¹å¯å¨äºã ä»å¤©éº»ç¦å¾å¤ï¼æå·²ç»æ²¡æèå¿äºã å®çå°ç¨é¿ï¼æ»å»åè¯å¥½ã è¿æ¯ä¸ä¸ªæ°çºªå½ï¼ è¿å¾æå¥åï¼ è¿æ¯ä¸ªå¾æ·±å¾æ·±çè£å£â¦â¦ è¿æ¾ç¶å¾è®©äººä¸ç½åã ä¸¤é¢å¢ä¹é´è·³è·çæè½ã ç¡®å®æ¯è¿æ ·ã è¿ä½¿å¾ä½ å¯ä»¥åç´è¡è¿å°è¾¾ä¸å±ã ç¼ç¨æ¯è¾éº»ç¦ï¼ä½æ¯é£åªæ¯å°å°çä»£ä»·ã ç»è¿è¿å¿ï¼
ä¾¿æ¯åå¾ç±³æ ¼æå§å±æçè·¯äºã å»å¥å¥å°å±ä¹è·¯ è¦åä¸è·³ï¼åªéæä½ä¸æ¹åé®ï¼
èåæè·³è·é®ã é¦åè·³åå¢ã è®¿é® http://www.frogatto.com/ äºè§£æ´å¤ å°±ææç¥éçï¼æ¯çã è¿ç¨é½ä¼å¨ä½ è¿å¥æä¸ªæ´æé´ä¹åæ®çä¸
æ¥ã åä½ è¯´çä¸æ ·ï¼æä¹è¦å¼å¿çåè¿äºã åè ä»£ç ç¿»è¯ ç¿»è¯åè°å å°è¯åææç§ç±»çæäººã è¦æé±æ¿åæ¥ã ç¼ ç»ç»¿æ  åï¼
ä½ åç¡®å®å¾è½æåï¼ åï¼ææ³ä½ è¯´çä¹å¯¹ã åã åã
åå¤´åæ²¡ææã åâ¦ä¸ºä»ä¹ä¼é£æ ·ï¼ å¦ï¼é£æå°±ä¸å®¢æ°äºâ¦
çæè°¢ä½ çæå¾ï¼ åï¼åï¼æå¬å° ... è¯¡å¼çå£°é³ã è¿ä¸ªæä½ å¸¦è¿ä¸ä¸ªæ¡¥ï¼ç©¿è¿å°ä¸ééã åâ¦ å...
é£æ¯ä¸ªè¦çå¿çåå³è¯... åâ¦ä½ å¥½ï¼ åï¼å¥½ç...
ä¸ç®¡ææ ·ï¼æä»¬æä¹åï¼ å...
æè¿ä»¥ä¸ºæ¯ä»ä¹æ´...é¾ç¼ çä¸è¥¿ã å... è¿æ ·ï¼æä¸æç½è¿ä¸ªè®¡åçç¨å¤ã å...ä½ æå°å¾ä¸ç±»çç©åï¼è½å¸®ææ¾ä¸ªï¼ åâ¦â¦å¥äºï¼ å°ä¸æ°´æ±  ä¸å¬å¹³ å¾ä¸å¹¸æä¸ç¥éã
æåªæ¯æ¥åè§çã å¨åæ¹ä½ ä¼çå°ä¸åº§èä¸ï¼
é£éä¼æºæºä¸æ­å°è·åºèèã æ´æ°é³ä¹ã ä¸æ®µç²æ¿ ç¨æ·çé¢ä»£ç  ä»iPhoneç1.01çæ¬ï¼ 1.0.2çæ¬ï¼ 1.0.3çæ¬ï¼ çæ¬ 1.1.1: çæ¬ 1.1 1.2 çæ¬: å¾å¥½ã èå©ï¼ åï¼ ç­ä¸ä¸ï¼ä½ å°±æ¯é¿åï¼ ç­ä¸ï¼
ä»ä»¥ä¸ºä½ å°±æ¯è¿éçé¦é¢ï¼ ç­ç­ï¼ä½ è¯´å¥ï¼ç±³æ ¼æå§ï¼ èµ°å°é¨æäººçåé¢ï¼åæä¸ è¿èè¿é æç¡çäºåï¼ å¨é£éé¢å°å¿æèè¢ã è¿æ°´å°å¾ æ¥¼ä¸æä¸ªæ§å¶é´ï¼
æä¸ªå¤§æé®å¯ä»¥æå¼é¨ã æä»¬è¿ç¥éç±³æ ¼æå§ä¸è¦è¿ä¸ªéå­ï¼å ä¸ºä»å é¢äº
åæ¾å¼äºã è¿ä¹ä¸éã å¾å¿ã
...ä¸å¥½ææï¼æå¾åå»å¹²æ´»äºã æä»¬ä¸ç¥å°é±çäºææ¯å¦å±å®ï¼
åè®¾ä¸ºçï¼ä»ä¹æ²¡ç¨ä¸ã ä½ ä¸æ¯ç¬¬ä¸ä¸ªè¿æ ·æ³çã æä»¬ç¥éä»ç¡®å®è¿ä½ç±³æ ¼æå§ æä»¬é©¬ä¸å»ã
ç¸ä¿¡è½æåºæ¥ï¼ æä»¬ä¹è½æä¸äºï¼ä¸è¿ç«å»å°±ä¼è¢«åå´ã æä»¬è¿å¿è¦ä¸ªäººå¸®ææã æä»¬çå°ç±³æ ¼æå§çå¯å®è¿å»äºã å¸¸èç³» - å¸æï¼
éè¿ä½ ï¼æä¹ç¥éäºéä¸ä¹æå¬æ­£çæåã æä»¬è§å¾æä»¬æ³è¦éä½£ä½ ä»¬åå³åï¼åä½ ä»¬
è½å¹²çã å°±åæ¯â¦â¦
å¬çåè´§è¿ç«è½¦é¡¶å¤´ã æä»¬å«âéèââ¦
å¤§æ¦æäºå¨æ´ç©´éä½ï¼ä½æ¯æä¸ã æä»¬è¿æ­£å¨å»ºé åå¡ã
ä½ è¦å»é£è¾¹çè¯å¾å°å¿ã æä»¬æ¯åèèã æä»¬é½è®¤è¯å¤å¹´äºï¼ä»æ¯ä¸ªåå¬åã æä»¬ä»æ²¡è§ä»çªç¶æå¦æ­¤å¤é±è±åºå»ï¼ä»ç¯
äºè¿æå¯è½ã æä»¬ç»ä»ä¸ä¸ªæ èçå·¥ä½ï¼
å¨æéä¸é¢çé¨ã æä»¬â¦â¦æä»¬æ²¡æ¦ä½ä»â¦â¦
ä½ä»â¦â¦ ç½ç«ç®¡çå å¥æªå¢ã
å®ä»¬æ³è¦ä»ä¹ï¼ è¹è··...è¿ä¸åºè¯¥åï¼
ä¸ºå¥å®ä»¬è®©ä½ ç¦»å¼ï¼ åï¼æè¿ä»¥ä¸ºä½ ä¼åè´¹
å¸®æåçå¿å¢ï¼ åï¼
è¿å±å¼å¯æ¯æåæ³çè¦å¥½å¢ã é¢ï¼ææ¯è¿éçÂ·é¦é¢ï¼è³å°ï¼è¿ä¸ªèä½ä¸è¬
å¯åº¶ã æè½ä½ çä¿è¯ä¼ éæåï¼ åï¼æä¸è½è®©ä»»ä½äºº
éè¿è¿æé¨ã ææ³æä»¬æä»çä½ç¶äº²ï¼
ä½æ¯æä»¬è¿æ¯å¶é çã ææ³æä»¬åºè¯¥æ­¦è£èåèµ·æ¥ï¼å¯¹æç±³æ ¼æ
å§ï¼ åï¼ä½ æçï¼å°éæ¯
åæä¸¤åçã åï¼æå»ï¼è¿é¨éä¸äºã å¥½è¿ï¼
æä¸ç´å¨è¿éè¿ã å¥½å§ï¼ä¼è®¡ï¼ä½ ä¿éã å¦æä»é¨å¸¦çæ­¦å¨ï¼
ä¸è¬æ¯ä¿å®çã åå¦ä½ ççæ¯ä¸ªè§ä¼ï¼æè®¸æè¯¥åä½ è´
æã å¥½å§ï¼è¿æ ·è¯´ï¼æä¸è½ä¾å¤ã
ç¡®å®ä¹æ¯ä¸ªå¥½çæ¶èææ¯ã å¦ï¼å¦æä½ æ³è¦æ¢æï¼æåçå°å®ä»¬çä¸ä¸ª
å£«å®è¿å¥èå¤´çå±å­ã é¢ï¼è¿ççæ¯ä»ä»¬çç®çåï¼ æå¸ææ¯å¾å¿«ã
æçæ¶åè±ä¸æ®µæ¶é´ã ååï¼æçæè°¢ã åµåµï¼è¿å¯¹æç®åå¤äºã
ä½ ä»¬ä¸¤ä¸ªå¥½å¥½èèã å¯ï¼ææ²¡ææ­¦å¨ï¼
æ¯ä¸ä¸ªå¾å¤§çåå ã åï¼ä»ä»¬è®©æåºæ¥äºã è¿å°æºå¨æ¯ä¿®è¾¹æºï¼å¨éå±ä¸­é»å­ï¼æå®ç ç£¨æåç§å½¢ç¶ã æä»¬éè¦äºå¸®æã ä¸ç®¡ä½ æä¹åå°çï¼ä»ä»¬èµ°äºå°±æ¯å¥½äºï¼ åï¼å¯¹ï¼
æç¡®å®æ¯è¦è¿å»ã å¥½å§ï¼ä½ åå°äºï¼ ä½ å¨åä»ä¹ï¼è¿æä½ æ¯è°ï¼ æä»¬è®¡åå¯¹ä»æ´ä¸ª ç±³æ ¼æå§ åéï¼ ä½ ç©¶ç«æ¯åä»ä¹çï¼ ä½ æ¾ä»æä»ä¹æ¯ï¼ èå¤´åè¯ä½ ä»ä¹ï¼ ä½ ä»ä¹ææï¼ é£æ¯ä»ä¹ææï¼ è¿å¥â¦â¦ ä½ å°åºæ³è¦ä»ä¹ï¼ ä½ çæææ¯ï¼è¿äºé±æ¯èåè¾
é®ä½ åçï¼ éè¦çæ¯æä¸åç¬¼å­éï¼æä»¥æå¯ä»¥å¸®å¿ã æ¥ä¸æ¥æä»ä¹ä»»å¡ï¼ æä¹äºï¼ï¼ ä»ä¹?ï¼
å®ä»¬ ... ç±³æ ¼æå§çç«ï¼ æè¿æ¥çæ¶åï¼
é¨å¨æèåå³ä¸äºã å½ä»è¢«é£ç«æä¸æ¥æ¶ï¼æä»¬èå¯¹è¿å»ä¸ä¼
å¿ã é¡ºä¾¿ä¸æï¼ä½ åå¼å¢ï¼ æçæ¯ä¸ºæéå»ºè®¾ä»ä¹ä¸è¥¿æå¥çã èªææ»¡è¶³çä»å¦æ­¤ä¹æè±äºéº»ç¦ã ææ¶åå°±æ¯ä¸ç§çé¢æ³çåçå¢ï¼ å½å®ä»¬åèæå¤©æ¶ï¼
å¾å®¹æåå°è¸©å»çä¼¤å®³ã å¨è´´ä½å²©å£çæ¶åï¼
æä¸è·³è·é®å°±å¯ä»¥è·³èµ·ã ä½ æ¯è°ï¼ é¬¼ç¥éï¼
ä¹è®¸æå¥½äºåçï¼ ä½ ä»å¦çæ¯è°ï¼ è°æ¥äºï¼ ååâ¦â¦
â¦â¦è¿æ¯æææ³çé£ä¸ªä¸è¥¿ä¹ï¼ ä»ä¹äºé½è¦æå¨æï¼ï¼ ä¸ºä»ä¹æä»¬ä¸ä»è¿éå¥æï¼
é£å®¶ä¼é¦åé®éé±ï¼ ä¸ºä»ä¹ï¼æä¼ç»ä½ ä¹è·¯ã èéå¥è§ å¥½å§ãåå¤´è§ï¼ ç°å¨ä½ è½å¬æé®è¯ä¹ï¼ èå©ï¼ Windows æåè æ²¡è¿æé¥åï¼
ä½ åªå¿é½å»ä¸äºã ååï¼é£å¨å¤©ä¸çæ¤ç©ï¼ ä¸çå°å¾ è¦æ¯åºç°æç³ç³çæåµäºï¼
æä¹åªè¦å¼æºå°±å¥½ãå¯¹å§ï¼ ä¹è®¸ä½ å¯ä»¥åè¿ä¸ªè°æ¥åï¼ æå»ºè®®åï¼ åï¼å®ä»¬å°±æ¯ç®åçæ´è£ç¦»å¼äºï¼
å¤ªå¥½äºï¼ åï¼å¯...
æç¡®å®æ³å¸®å©ï¼ä½æ¯...
æä¸æç½èªå·±è½åä»ä¹ã åï½ä½ ä»¬ä¿©å®¶ä¼çä¸å»å®å¨ä¸æ ·ã åï½ å¦ï¼é£å¾åé©åï¼ æäº â¢ åä¸ä¸»è¦æäºåå¤§å¤å¯¹è¯ ä¸éï¼ æ¯åï¼æ³å»é£æ¶æè¿ä»¥ä¸ºåä¼æ¯æ èçä¸å¤©ï¼
  å¥½ï¼æä¸è½å¸®ä½ ï¼ä¸è¿å¬å¤äºæ¶æ¯...å¥½æ¶æ¯
æ¯ï¼ä½ çé®é¢è§£å³äºï¼ æ¯åï¼æå°±å¸æä¸è¦éå¤è¿æ ·çäºä»¶â¦ å¦ï¼ææçãæè¯è¯çã å¯ï¼æä»¥ä¸ºä½ æ¯ä»æ´ç©´
è·åºçæç§æªç©ã å¥½ï¼æå»çä¸ä¸ã æ¯ï¼ææç¹æå¿§è¥æ¯æ²¡äººæ¥è§¦ç±³æ ¼æå§æåµ
ä¼ææ ·ã å¯ï¼ä¸åã å¥½ï¼æ²¡é®é¢ã
é£ä¹ï¼åè§ï¼è°¢å¦ï¼ æ¯ï¼æä¸ªå°å¤´ç®ç»æçäºä¸å¹ä»çå¾åã æ¯ï¼æè¿äºå°ç¦å©ï¼çæ´»æºèæã æ¯ï¼æå¬å°ä¹ä¸æªã æ¯åï¼ä½ èº«è¾¹é£é¥åââ
ææ³æ¯è½æå¼æå±å­çé¨çï¼
é£å±å­éæå°éå¤§é¨çé¥åã æ¯ï¼è¿æ¯ä¸ä¸ªå®¿èåºã
è¿éä½çå¾å¤äººã æ¯ï¼æ´å¤©é½ä¸å¹³å¸¸äºã æ¯åï¼é£äºè ¢è
çåä¼æ²¡å¥ç¨ã å¥½ã ä¸éã
æ®å¿µçæ¯ææä¸å¼è¿éã
æå¥é½æ²¡æã æ©ã
åæé£å®¶ä¼æ¯è°ï¼ ä¹æ¯ãæä»¬ç®åä¸ºæ­¢å°±ç¥éï¼ æ¯ãæä¸ç¥éå³äºè£å£æçäºæï¼å¦æä½ ä»¬åè°æï¼æçå°è±¡
æ¹åï¼ææ³å¯ä»¥å¸®ä¸äºã å¥½...ç©¿è¿...å±±æ´ï¼ç°å¨ï¼
è¿æ´ç©´é¿ä¸é¿ï¼ å¯¹å¤´ã å° æè¦å»ï¼ç°å¨è½¬åå»ææ´åç¬¨èã
  å¯ï¼ææ³é£æ³¢ä¹åæ¯ä¸ªäººé½æ³è¦åæ ·çç»
æã æ¯ï¼ææ¶å°äºä¸äºè´é¢çæ¥åï¼å³äºè´¢ç©æ
æ¯ã æ¯ï¼ææ¾äºäºçäººâ¦ ä¸è¿ï¼ä»ç¡®å®æ¯æ­£å¼çæ¥å¾åã æ¯åï¼è¿æ¯é¥åã
ä¸é¢æäºèèç¬æ¥ç¬å»çã æ¯ï¼ä½ å¥½ã æç¡®å®æ¯ã æ¯çã
æä»¬æä¼ç­åå·¥çå®æ¶é¤ã æ¯åï¼æææä»ä¹ææé©¾å°ï¼
æ¬å°±æ¯ä¸é è°±çã æ¯åï¼åç»å£«ä¸æ ·ã
ä¸è¾¹åè¶ä¸è¾¹è°ã å¥½å§...
ä»ä»¬åªæ¯å¼ºçï¼ç®åæäºã ä¸éââæä»¬æ¯âå¶é âçã
ä¸æ¯åä½ ä¸æ ·åºççã æ¯ï¼è¿ä¸ªå³å¤´ï¼æä¸æ¯å¾éè¦ã å¯ä»¥ã æ¯ã
å°±æ¯å®ã æ¯... æä¹åªè½è¿æ ·äºï¼
ä¸è¿å¨é£å¿è¦å°å¿ç¹ã åâ¦
ææç½ä½ çææã å½ç¶ã å½ç¶ã ä½ ä¹åæ ·å¯ä»¥åä¸è·³ç©¿è¿å®ä»¬ã ä½ å¯ä»¥æ¶ééå¸
ä»¥åå¶ä»éè´¨ç©åï¼
èåç¨æ¥è´­ä¹°è£å¤ï¼ ä½ å¯ä»¥ç¨èå¤´å·ä½æäººåä¸å»ï¼
èååå°å®ä»¬ååºå»ã ä½ ç°å¨å¯ä»¥ä»ææå°å°åºæå®³ç
ç«çï¼å®ä»¬ä¼è·è¸ªæ»å»æäººã ä½ ç°å¨å¯ä»¥ä»æå°ååº
å·æä¼¤å®³åçè½éç«çã ä½ å¯ä»¥ç¥è¿å¯¹è¯ï¼åæ¬é¨ååºæ¯çæµç¨ï¼æ
ä¸ "ESC" å³å¯ã ä½ çé±ä¸å¤ã ççåï¼ï¼ ä½ æ²¡æ³å°ä»è½æ¹åå§ï¼ ä½ æ¾å°ä¸ä¸ªé»éçºªå¿µå¥ï¼ ä½ å­¦ä¼äºæ°çè½åï¼ ä½ å¨æ¹åè·éå¾äº${level.player.score}åï¼ ä½ å¨æ·±å¥ååå¤§åéå¾äº${level.player.vars.score}åï¼ ä½ å¨åæå·¥åè·å¾ ${level.player.vars.score} åï¼ ä½ å¨èéå¥è§éå¾äº${level.player.vars.score}åï¼ ä½ å¨é¿éè¿è¾éå¾äº${level.player.vars.score}åï¼ ä½ å¨ç»´ä¿®ç®¡ééå¾äº${level.player.vars.score}åï¼ ä½ å¨ä¸æ®µç²æ¿éå¾äº${level.player.vars.score}åï¼ ä»ä»¬â¦ææééçäººåå¾å¤åã ä½ å¾å°äºä¸ä¸ªçº¢å¿è¶åã
ä½ çæå¤§çå½å¼å¢å äºä¸ï¼ è¯´å®è¯ââææè§æºæªï¼
ä¸ä¼è·ä½ ä»¬ææï¼ç¶åâ¦ æå°è¿ä¸ªï¼ä½ æºé¢çã ä½ çèµ·æ¥æ¯ä¸ª
å¾å¯ççè§è²ã ä½ è¾¾å°äºç¡¬å¸ééèµå°ç¬¬ ${level.player.difficulty} å³ã å¯¹ä¸èµ·ï¼æå®å¨å¾å¿ã
ç°å¨æ²¡ææ¶é´è°è¯ã ä½ å¾æ©ç¹èµ·åºææ¯ã ä½ å¯è½ç¥éå¶ä»äººçå´è¶£ï¼ä¸è¿æçå»ºè®®è¯å®ææ£ï¼
é ä¸æ¡ç¸äºè¿éçéè·¯ã æ³¨æä¸æ¯æææäººé½è½ç¨èå¤´æä½ã è¿ä¹è¯´æ¥ï¼ä½ è¿æ²¡å¬è¯´åã ä½ ï¼ï¼ ä½ ä¼æ¾å°å¾å¤ç±»ä¼¼å¤§å°çä¸è¥¿éå¸æ¸¸æè§è½ï¼
æ¯å¦ç®±å­ï¼æ ä¸çåæã ä½ ä¸ªå¥ä¾µèï¼åï¼ ä½ è¦æ¿æçè¯ï¼å¯ä»¥åä¸æ¥ä¸èµ·è¯»ã
çç«å¾èæã ä¸ç¨è°¢ï¼ä¸è¿ä½ è¯´çè¿æç¹æ©â¦
ææä¸æ ·ä¸è¥¿å¸æè½å¸®ä¸å¿ã ä¸å®¢æ°ã ä½ æ¶éé½äºç¢çï¼è¶³ä»¥ç»æå®æ´ççº¢å¿è¶åï¼
ä½ çæå¤§çå½å¼å¢å äºä¸ï¼ ä½ æ¾å°äºçº¢å¿è¶åçä¸é¨åã
åæ¶éä¸åç¢çå°±å¯ä»¥ä½¿
ä½ çæå¤§çå½å¼å¢å ä¸ï¼ ä½ æ¾å°äºçº¢å¿è¶åçä¸é¨åã
åæ¶éä¸åç¢çå°±å¯ä»¥ä½¿
ä½ çæå¤§çå½å¼å¢å ä¸ï¼ ä½ æ¾å°äºçº¢å¿è¶åçä¸é¨åã
åæ¶éä¸¤åç¢çå°±å¯ä»¥ä½¿
ä½ çæå¤§çå½å¼å¢å ä¸ï¼ ä½ çå°çªï¼ æå¤§çé®é¢æ¯è¿æ¥çå°éä¸¤åç
å¤§é¨è¢«éä¸äºã å¯¹å¤´ï¼æå°±æ¯é¿åï¼ .^æ¯ æ¬æ¥æ¯è¦ç¨æ¥å¨éä¸æå»ºè®¾çï¼ ä½æ¯æåå¨è¿éåä¸ªç¬¨èä¸æ ·ï¼
å¨å°ä¸ç»ååã ä¸è¿è¿å°±æ¯ä¸é´çååã ç©ºç è½éæ°´æ¶ æ»ç¿ç¿¼ ä½ â¦ä½ å¥½ï¼ çº¢å¿ ååºæ°´æ¶ æ ææ°´æ¶ keywords: platformer,pixelart,oldschool,platform,game,pixel,arcade,action,adventure,frog å·åå å¼º ç»é¢ä¸åºç°çæé®ï¼å°±å¯ä»¥å¼é¨æäº¤è°ã èå¤´å é¿ 